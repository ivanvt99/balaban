<?php

use \App\Http\Controllers\Admin\PermissionController;
use \App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\DailyMealController;
use App\Http\Controllers\DietController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InquiryController;
use App\Http\Controllers\MealController;
use App\Http\Controllers\ProductIngredientController;
use App\Http\Controllers\RecipeController;
use App\Http\Controllers\SupplementController;
use App\Http\Controllers\SupplementPortionController;
use App\Http\Controllers\TrainerController;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\UserController;
use \App\Http\Controllers\FoodProductController;
use \App\Http\Controllers\FoodCategoryController;
use \App\Http\Controllers\MuscleGroupController;
use \App\Http\Controllers\ExerciseController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () {
    return view('frontend.homepage');
});
Route::get('/about_us', function () {
    return view('frontend.about_us');
});

Route::group(['prefix' => 'nutrition'], function () {
    Route::get('/display_recipes', [FrontendController::class, 'recipeCatalogue'])->name('nutrition.recipeCatalogue');
    Route::get('/display_diets', [FrontendController::class, 'dietsCatalogue'])->name('nutrition.dietsCatalogue');
    Route::get('/{id}', [FrontendController::class, 'showRecipe'])->name('nutrition.showRecipe');
    Route::get('/diet/{id}', [FrontendController::class, 'showDiet'])->name('nutrition.showDiet');
    Route::get('/meal/{id}', [FrontendController::class, 'showMeal'])->name('nutrition.showMeal');
});

Route::group(['prefix' => 'users'], function () {
    Route::get('/', [UserController::class, 'index'])->name('users.index');
    Route::get('/create', [UserController::class, 'create'])->name('users.create');
    Route::post('/', [UserController::class, 'store'])->name('users.store');
    Route::get('/edit/{id}', [UserController::class, 'edit'])->name('users.edit');
    Route::put('/update/{id}', [UserController::class, 'update'])->name('users.update');
    Route::delete('/delete/{id}', [UserController::class, 'delete'])->name('users.delete');
});


Route::group(['prefix' => 'food_products'], function () {
    Route::get('/', [FoodProductController::class, 'index'])->name('food_products.index');
    Route::get('/create', [FoodProductController::class, 'create'])->name('food_products.create');
    Route::post('/', [FoodProductController::class, 'store'])->name('food_products.store');
    Route::get('/edit/{id}', [FoodProductController::class, 'edit'])->name('food_products.edit');
    Route::put('/update/{id}', [FoodProductController::class, 'update'])->name('food_products.update');
    Route::delete('/delete/{id}', [FoodProductController::class, 'delete'])->name('food_products.delete');
});

Route::group(['prefix' => 'food_categories'], function () {
    Route::get('/', [FoodCategoryController::class, 'index'])->name('food_categories.index');
    Route::get('/create', [FoodCategoryController::class, 'create'])->name('food_categories.create');
    Route::post('/', [FoodCategoryController::class, 'store'])->name('food_categories.store');
    Route::get('/edit/{id}', [FoodCategoryController::class, 'edit'])->name('food_categories.edit');
    Route::put('/update/{id}', [FoodCategoryController::class, 'update'])->name('food_categories.update');
    Route::delete('/delete/{id}', [FoodCategoryController::class, 'delete'])->name('food_categories.delete');
});

Route::group(['prefix' => 'muscle_groups'], function () {
    Route::get('/', [MuscleGroupController::class, 'index'])->name('muscle_groups.index');
    Route::get('/create', [MuscleGroupController::class, 'create'])->name('muscle_groups.create');
    Route::post('/', [MuscleGroupController::class, 'store'])->name('muscle_groups.store');
    Route::get('/edit/{id}', [MuscleGroupController::class, 'edit'])->name('muscle_groups.edit');
    Route::put('/update/{id}', [MuscleGroupController::class, 'update'])->name('muscle_groups.update');
    Route::delete('/delete/{id}', [MuscleGroupController::class, 'delete'])->name('muscle_groups.delete');
});

Route::group(['prefix' => 'exercises'], function () {
    Route::get('/', [ExerciseController::class, 'index'])->name('exercises.index');
    Route::get('/create', [ExerciseController::class, 'create'])->name('exercises.create');
    Route::post('/', [ExerciseController::class, 'store'])->name('exercises.store');
    Route::get('/edit/{id}', [ExerciseController::class, 'edit'])->name('exercises.edit');
    Route::put('/update/{id}', [ExerciseController::class, 'update'])->name('exercises.update');
    Route::delete('/delete/{id}', [ExerciseController::class, 'delete'])->name('exercises.delete');
});

Route::group(['prefix' => 'product_ingredients'], function () {
    Route::get('/', [ProductIngredientController::class, 'index'])->name('product_ingredients.index');
    Route::get('/create', [ProductIngredientController::class, 'create'])->name('product_ingredients.create');
    Route::post('/', [ProductIngredientController::class, 'store'])->name('product_ingredients.store');
    Route::get('/edit/{id}', [ProductIngredientController::class, 'edit'])->name('product_ingredients.edit');
    Route::put('/update/{id}', [ProductIngredientController::class, 'update'])->name('product_ingredients.update');
    Route::delete('/delete/{id}', [ProductIngredientController::class, 'delete'])->name('product_ingredients.delete');
});


Route::group(['prefix' => 'admin'], function () {
    Route::get('/roles', [RoleController::class, 'index'])->name('admin.roles.index');
    Route::get('/roles/create', [RoleController::class, 'create'])->name('admin.roles.create');
    Route::post('/roles', [RoleController::class, 'store'])->name('admin.roles.store');
    Route::get('/roles/edit/{role:name}', [RoleController::class, 'edit'])->name('admin.roles.edit');
    Route::patch('/roles/edit/{role:name}', [RoleController::class, 'update'])->name('admin.roles.update');
    Route::delete('/roles/delete/{role:name}', [RoleController::class, 'destroy'])->name('admin.roles.delete');

    Route::get('/permissions', [PermissionController::class, 'index'])->name('admin.permissions.index');
    Route::get('/permissions/create', [PermissionController::class, 'create'])->name('admin.permissions.create');
    Route::post('/permissions', [PermissionController::class, 'store'])->name('admin.permissions.store');
    Route::get('/permissions/edit/{permission:name}', [PermissionController::class, 'edit'])->name('admin.permissions.edit');
    Route::put('/permissions/edit/{permission:name}', [PermissionController::class, 'update'])->name('admin.permissions.update');
    Route::delete('/permissions/delete/{permission:name}', [PermissionController::class, 'destroy'])->name('admin.permissions.delete');

});

Route::group(['prefix' => 'trainers'], function () {
    Route::get('/', [TrainerController::class, 'index'])->name('trainers.index');
    Route::get('/create', [TrainerController::class, 'create'])->name('trainers.create');
    Route::post('/', [TrainerController::class, 'store'])->name('trainers.store');
    Route::get('/edit/{id}', [TrainerController::class, 'edit'])->name('trainers.edit');
    Route::put('/edit/{id}', [TrainerController::class, 'update'])->name('trainers.update');
    Route::delete('/delete/{id}', [TrainerController::class, 'delete'])->name('trainers.delete');
});
Route::group(['prefix' => 'clients'], function () {
    Route::get('/', [ClientController::class, 'index'])->name('clients.index');
    Route::get('/create', [ClientController::class, 'create'])->name('clients.create');
    Route::post('/', [ClientController::class, 'store'])->name('clients.store');
    Route::get('edit/{id}', [ClientController::class, 'edit'])->name('clients.edit');
    Route::put('update/{id}', [ClientController::class, 'update'])->name('clients.update');
    Route::delete('/delete/{id}', [ClientController::class, 'delete'])->name('clients.delete');
});

Route::group(['prefix' => 'recipes'], function () {
    Route::get('/', [RecipeController::class, 'index'])->name('recipes.index');
    Route::get('/create', [RecipeController::class, 'create'])->name('recipes.create');
    Route::post('/', [RecipeController::class, 'store'])->name('recipes.store');
    Route::get('edit/{id}', [RecipeController::class, 'edit'])->name('recipes.edit');
    Route::put('update/{id}', [RecipeController::class, 'update'])->name('recipes.update');
    Route::delete('/delete/{id}', [RecipeController::class, 'delete'])->name('recipes.delete');
});

Route::group(['prefix' => 'supplements'], function () {
    Route::get('/', [SupplementController::class, 'index'])->name('supplements.index');
    Route::get('/create', [SupplementController::class, 'create'])->name('supplements.create');
    Route::post('/', [SupplementController::class, 'store'])->name('supplements.store');
    Route::get('edit/{id}', [SupplementController::class, 'edit'])->name('supplements.edit');
    Route::put('update/{id}', [SupplementController::class, 'update'])->name('supplements.update');
    Route::delete('/delete/{id}', [SupplementController::class, 'delete'])->name('supplements.delete');
});

Route::group(['prefix' => 'supplement_portions'], function () {
    Route::get('/', [SupplementPortionController::class, 'index'])->name('supplement_portions.index');
    Route::get('/create', [SupplementPortionController::class, 'create'])->name('supplement_portions.create');
    Route::post('/', [SupplementPortionController::class, 'store'])->name('supplement_portions.store');
    Route::get('edit/{id}', [SupplementPortionController::class, 'edit'])->name('supplement_portions.edit');
    Route::put('update/{id}', [SupplementPortionController::class, 'update'])->name('supplement_portions.update');
    Route::delete('/delete/{id}', [SupplementPortionController::class, 'delete'])->name('supplement_portions.delete');
});

Route::group(['prefix' => 'meals'], function () {
    Route::get('/', [MealController::class, 'index'])->name('meals.index');
    Route::get('/create', [MealController::class, 'create'])->name('meals.create');
    Route::post('/', [MealController::class, 'store'])->name('meals.store');
    Route::get('edit/{id}', [MealController::class, 'edit'])->name('meals.edit');
    Route::put('update/{id}', [MealController::class, 'update'])->name('meals.update');
    Route::delete('/delete/{id}', [MealController::class, 'delete'])->name('meals.delete');
});

Route::group(['prefix' => 'daily_meals'], function () {
    Route::get('/', [DailyMealController::class, 'index'])->name('daily_meals.index');
    Route::get('/create', [DailyMealController::class, 'create'])->name('daily_meals.create');
    Route::post('/', [DailyMealController::class, 'store'])->name('daily_meals.store');
    Route::get('/{id}', [DailyMealController::class, 'show'])->name('daily_meals.show');
    Route::get('edit/{id}', [DailyMealController::class, 'edit'])->name('daily_meals.edit');
    Route::put('update/{id}', [DailyMealController::class, 'update'])->name('daily_meals.update');
    Route::delete('/delete/{id}', [DailyMealController::class, 'delete'])->name('daily_meals.delete');
});

Route::group(['prefix' => 'diets'], function () {
    Route::get('/', [DietController::class, 'index'])->name('diets.index');
    Route::get('/create', [DietController::class, 'create'])->name('diets.create');
    Route::post('/', [DietController::class, 'store'])->name('diets.store');
    Route::get('/{id}', [DietController::class, 'show'])->name('diets.show');
    Route::get('edit/{id}', [DietController::class, 'edit'])->name('diets.edit');
    Route::put('update/{id}', [DietController::class, 'update'])->name('diets.update');
    Route::delete('/delete/{id}', [DietController::class, 'delete'])->name('diets.delete');
});

Route::group(['prefix' => 'inquiries'], function () {
    Route::get('/', [InquiryController::class, 'index'])->name('inquiries.index');
    Route::get('/create', [InquiryController::class, 'create'])->name('inquiries.create');
    Route::post('/', [InquiryController::class, 'store'])->name('inquiries.store');
    Route::get('/{id}', [InquiryController::class, 'show'])->name('inquiries.show');
    Route::get('/edit/{id}', [InquiryController::class, 'edit'])->name('inquiries.edit');
    Route::put('/update/{id}', [InquiryController::class, 'update'])->name('inquiries.update');
    Route::delete('/delete/{id}', [InquiryController::class, 'delete'])->name('inquiries.delete');
});
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/dashboard', [HomeController::class, 'dashboard'])->name('dashboard');
Route::get('/welcome', [HomeController::class, 'website'])->name('welcome');
