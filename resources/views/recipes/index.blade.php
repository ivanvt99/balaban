@extends('adminlte::page')

@section('title', __('Product Option Values').' - '.env('APP_NAME'))

@section('content')
    <h1 class="text-center">{{__('Recipes')}}</h1>
    <div class="row justify-content-center">
        <div class="col-12">
            @can('recipe-create')
                <a href="{{route('recipes.create')}}"
                   class="btn btn-primary mb-3 float-right">{{__('Create New')}}</a>
            @endcan
            <table class="table table-dark">
                <tr>
                    <th>{{__('Photo')}}</th>
                    <th>{{__('ID')}}</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('Description')}}</th>
                    <th>{{__('Ingredients')}}</th>
                    <th>{{__('Preparation Time')}}</th>
                    <th>{{__('cooking Time')}}</th>
                    <th>{{__('Calories per normal meal (400g)')}}</th>
                    <th>{{__('Actions')}}</th>
                </tr>
                @foreach($recipes as $recipe)

                    <tr>

                        <td>
{{--                            @dd($recipe->getMedia('recipe/'.$recipe->id)->last()->getUrl())--}}
                            @if($recipe->getMedia('recipe/'.$recipe->id)->count() > 0)
                                <img
                                    src="{{$recipe->getMedia('recipe/'.$recipe->id)->last()->getUrl()}}"
                                    style="width: 100px">
                                </a>
                            @else
                                {{__('No Data')}}
                            @endif
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            @if($recipe->id)
                                {{$recipe->id}}
                            @else
                                {{__('No Data')}}

                            @endif
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            @if($recipe->name)
                                {{$recipe->name}}
                            @else
                                {{__('No Data')}}

                            @endif
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            @if($recipe->description)
                                {{$recipe->description}}
                            @else
                                {{__('No Data')}}

                            @endif
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            @foreach($recipe->productIngredients as $ingredient)
                                {{$ingredient->foodProduct->name.' - '.
                                $ingredient->amount. ' '. $ingredient->amount_type}}
                                <br>
                            @endforeach
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            @if($recipe->preparation_time)
                                {{$recipe->preparation_time.' '.$recipe->time_amount }}
                            @else
                                {{__('No Data')}}

                            @endif
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            @if($recipe->cooking_time)
                                {{$recipe->cooking_time.' '.$recipe->time_amount }}
                            @else
                                {{__('No Data')}}

                            @endif
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            {{$recipe->calories. ' '}} calories
                        </td>
                        <td class="d-flex" style="text-align: center; vertical-align: middle;">
                            @can('recipe-edit')
                                <a href="{{route('recipes.edit',$recipe->id)}}"
                                   class="text-decoration-none text-white mx-2">
                                    <button class="btn btn-primary">{{__('Edit')}}</button>
                                </a>
                            @endcan
                            @can('recipe-delete')
                                <form action="{{route('recipes.delete', $recipe->id)}}"
                                      method="post" class="d-inline" autocomplete="off">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit"
                                            onclick="return confirm('Are you sure you want to delete this item?');">
                                        {{__('Delete')}}
                                    </button>
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    {{--    {{ $recipes->links('pagination::bootstrap-5') }}--}}
@endsection
