@extends('adminlte::page')

@section('title', __('Create New Recipe'))
@section('js')
    <script>
        var loadFile = function (event) {
            var output = document.getElementById('image');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function () {
                URL.revokeObjectURL(output.src)
            }
        };
    </script>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 pt-3">
                <div class="card">

                    <h5 class="card-header">{{ __('Create New Recipe') }}</h5>

                    <div class="card-body">
                    <h3 class="text-center text-bold mb-4">{{__('Recipe Credentials')}}</h3>


                        <form method="post" action="{{route('recipes.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row mb-3">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>
                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror"
                                           name="name"
                                           value="{{ old('name') }}" required autocomplete="name">

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="description"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Description') }}</label>
                                <div class="col-md-6">
                                    <textarea cols="35" rows="5" id="description" type="text"
                                              class="form-control @error('description') is-invalid @enderror"
                                              name="description"
                                              value="{{ old('description') }}" required autocomplete="name"></textarea>
                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="ingredient"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Ingredients') }}</label>
                                <div class="col-md-6">

                                    <ul style="list-style-type: none" class="ingredients_selector">
                                        @foreach(\App\Models\ProductIngredient::all() as $ingredient)
                                            <li>
                                                <input type="checkbox" id="ingredient" name="ingredients[]" value="{{$ingredient->id}}">
                                            {{$ingredient->foodProduct->name. '-'.$ingredient->amount. ' ' .$ingredient->amount_type}}
                                            <li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="preparation_time"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Prep Time') }}</label>
                                <div class="col-md-6">
                                    <input id="preparation_time" type="text"
                                           class="form-control @error('preparation_time') is-invalid @enderror"
                                           name="preparation_time"
                                           value="{{ old('preparation_time') }}" required autocomplete="preparation_time">

                                    @error('preparation_time')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="cooking_time"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Cooking Time') }}</label>
                                <div class="col-md-6">
                                    <input id="cooking_time" type="text"
                                           class="form-control @error('cooking_time') is-invalid @enderror"
                                           name="cooking_time"
                                           value="{{ old('cooking_time') }}" required autocomplete="cooking_time">

                                    @error('cooking_time')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="time_amount"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Time Amount') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control @error('time_amount') is-invalid @enderror" name="time_amount" required>
                                        <option value="" selected disabled>{{__('Choose amount type')}}...</option>
                                        <option value="hours">{{__('Hour/s')}}</option>
                                        <option value="minutes">{{__('Minute/s')}}</option>
                                    </select>
                                    @error('time_amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="calories"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Calories per 1 meal(400g)') }}</label>
                                <div class="col-md-6">
                                    <input id="calories" type="text"
                                           class="form-control @error('calories') is-invalid @enderror"
                                           name="calories"
                                           value="{{ old('calories') }}" required autocomplete="calories">

                                    @error('calories')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-md-4 col-form-label text-md-end">{{__('Image')}}</label>
                                <div class="col-sm-6">
                                    <input type="file" name="image" id="file" accept="image/*"
                                           class="form-control-file @error('image') is-invalid @enderror"
                                           onchange="loadFile(event)"
                                           required>
                                    @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create New') }}
                                    </button>
                                </div>
                            </div>
                            <div class="row mb-0 justify-content-center">
                                <div id="images" class="col-12 text-center">
                                    <img id="image" style="width: 200px">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
