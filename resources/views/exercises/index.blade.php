@extends('adminlte::page')
@section('Title', __('Exercises'))

@section('content')
    <div class="container">
        <h1 class="text-center mt-3">{{__('Exercises')}}</h1>
        <div class="mb-3 mt-3 text-right">
            <a href="{{route('exercises.create')}}"
               class="btn btn-primary float-end">{{__('Create new')}}
            </a>
        </div>
        <div class="row justify-content-center">
            <table class="table table-dark">
                <thead>
                <th>{{__('ID')}}</th>
                <th>{{__('Muscle Group')}}</th>
                <th>{{__('Name')}}</th>
                <th>{{__('Description')}}</th>
                <th>{{__('Actions')}}</th>
                </thead>
                <tbody>
                @foreach($exercises as $exercise)
                    <tr>
                        <td>{{($exercise->id)}}</td>
                        <td>{{($exercise->muscleGroup->name)}}</td>
                        <td>{{($exercise->name)}}</td>
                        <td>{{($exercise->description)}}</td>

                        <td>
                            <div class="d-flex">
                                <div class="edit mx-2">
                                    <a href="{{route('exercises.edit', $exercise->id)}}" class="btn btn-primary">{{__('Edit')}}</a>
                                </div>
                                <form  method="post" action="{{route('exercises.delete', $exercise->id)}}" class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger"
                                            onclick="return confirm('Are you sure you want to delete this item?');">{{__('DELETE')}}</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
