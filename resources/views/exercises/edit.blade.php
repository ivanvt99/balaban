@extends('adminlte::page')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 pt-3">
                <div class="card">
                    <h5 class="card-header">{{ __('Create New Exercise') }}</h5>
                    <div class="card-body">
                        <form method="POST" action="{{route('exercises.update', $exercise->id)}}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row mb-3">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-end">{{__('Name')}}</label>

                                <div class="col-md-6">
                                    <input type="text"
                                           name="name"
                                           id="name"
                                           class="form-control" @error('name') is-invalid @enderror
                                           value="{{ $exercise->name }}" required
                                           autocomplete="name" autofocus>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="muscle_group_id"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Muscle Group') }}</label>

                                <div class="col-md-6">
                                    <select id="muscle_group_id"
                                            class="form-control @error('muscle_group_id') is-invalid @enderror"
                                            name="muscle_group_id"
                                            required autofocus>
                                        <option selected disabled>{{$exercise->muscleGroup->name}}...</option>
                                        @foreach(\App\Models\MuscleGroup::all() as $group)
                                            <option value="{{$group->id}}">{{ ucwords($group->name) }}</option>
                                        @endforeach
                                    </select>

                                    @error('muscle_group_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="description"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Description') }}</label>
                                <div class="col-md-6">
                                    <textarea cols="35" rows="5" id="description" type="text"
                                              class="form-control @error('description') is-invalid @enderror"
                                              name="description"
                                              value="{{$exercise->description }}" required autocomplete="name">{{  $exercise->description}}
                                    </textarea>
                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
