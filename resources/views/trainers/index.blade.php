@extends('adminlte::page')

@section('title', __('Trainers').' - '.env('APP_NAME'))

@section('content')
    <div class="row justify-content-center">
        <h1 class="text-center">{{__('Trainers')}}</h1>
        <div class="col-12">
            @if(session('error'))
                <div class="row justify-content-center">
                    <div class="alert alert-danger"> {{session('error')}}</div>
                </div>
            @endif
            @if(session('success'))
                <div class="row justify-content-center">
                    <div class="alert alert-success"> {{session('success')}}</div>
                </div>
            @endif
            @can('trainer-create')
                <a href="{{route('trainers.create')}}" class="btn btn-primary mb-3 float-right">{{__('Create New')}}</a>
            @endcan
            <table class="table table-dark">
                <thead>
                <tr>
                    <th> {{__('ID')}}</th>
                    <th>{{__('Trainer Name')}}</th>
                    <th>{{__('Address')}}</th>
                    <th>{{__('Email')}}</th>
                    @can('trainer-set-status')
                        <th>{{__('Status')}}</th>
                    @endcan
                    <th>{{__('Actions')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($trainers as $trainer)
                    <tr>
                        <td>
                            @if($trainer->id)
                                {{$trainer->id}}
                            @else
                                {{__('No Data')}}
                            @endif
                        </td>
                        <td>
                            @if($trainer->trainer_name)
                                {{$trainer->trainer_name}}
                            @else
                                {{__('No Data')}}
                            @endif
                        </td>
                        <td>
                            @if($trainer->address)
                                {{$trainer->address}}
                            @else
                                {{__('No Data')}}
                            @endif
                        </td>
                        <td>
                            @if($trainer->email)
                                {{$trainer->email}}
                            @else
                                {{__('No Data')}}
                            @endif
                        </td>
                        @can('trainer-set-status')
                            <td>
                                @if($trainer->status)
                                    {{$trainer->status}}
                                @else
                                    {{__('No Data')}}
                                @endif
                            </td>
                        @endcan
                        <td>
                            <div class="d-flex">
                                @can('trainer-edit')
                                    <a href="{{route('trainers.edit', $trainer->id)}}"
                                       class="btn btn-primary mx-2">
                                        {{__('Edit')}}
                                    </a>
                                @endif
                                @can('trainer-delete')
                                    <form action="{{route('trainers.delete', $trainer->id)}}"
                                          method="post" class="d-inline" autocomplete="off">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger" type="submit"
                                                onclick="return confirm('Are you sure you want to delete this trainer?');">
                                            Delete
                                        </button>
                                    </form>
                                @endcan
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
