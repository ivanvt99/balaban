@extends('adminlte::page')
@section('Title', __('Meals'))

@section('content')
    <div class="container">
        <h1 class="text-center mt-3">{{__('Meals')}}</h1>
        <div class="mb-3 mt-3 text-right">
            @can('meal-create')
                <a href="{{route('meals.create')}}"
                   class="btn btn-primary float-end">{{__('Create new')}}
                </a>
            @endcan
        </div>
        <div class="row justify-content-center">
            <table class="table table-dark">
                <thead>
                <th>{{__('ID')}}</th>
                <th>{{__('Meal Name')}}</th>
                <th>{{__('Recipes:')}}</th>
                <th>{{__('Additional Product')}}</th>
                <th>{{__('Supplement')}}</th>
                <th>{{__('Calories for the meal')}}</th>
                <th>{{__('Meal Notes')}}</th>
                <th>{{__('Actions')}}</th>
                </thead>
                <tbody>
                @foreach( $meals as  $meal)
                    <tr>
                        <td>{{$meal->id}}</td>

                        <td>{{( $meal->name)}}</td>
                        <td>
                            @foreach($meal->recipes as $recipe)
                                {{$recipe->name}}
                                <br>
                            @endforeach
                        </td>
                        <td>

                            @foreach($meal->productIngredients as $ingredient)
                                {{$ingredient->foodProduct->name.' '.$ingredient->amount.' '.$ingredient->amount_type }}
                                <br>
                            @endforeach
                            @if($meal->productIngredients()->count() == 0)
                                {{__('No additions')}}
                            @endif
                        </td>
                        <td>
                            @foreach($meal->supplementPortions as $portion)

                                {{$portion->supplement->name.' '.$portion->amount.' '.$portion->amount_type }}
                                <br>
                            @endforeach
                            @if($meal->supplementPortions->count() == 0)
                                {{__('No portion')}}
                            @endif

                        </td>
                        <td>{{$meal->meal_calories}}</td>
                        <td>
                            @if($meal->notes)
                                {{$meal->notes}}
                            @else
                                {{__('No notes available for the current meal')}}
                            @endif
                        </td>
                        <td>
                            <div class="d-flex">
                                @can('meal-edit')
                                    <div class="edit mx-2">
                                        <a href="{{route('meals.edit',  $meal->id)}}"
                                           class="btn btn-primary">{{__('Edit')}}</a>
                                    </div>
                                @endcan
                                @can('meal-delete')
                                    <form method="post" action="{{route('meals.delete',  $meal->id)}}" class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger"
                                                onclick="return confirm('Are you sure you want to delete this item?');">{{__('DELETE')}}</button>
                                    </form>
                                @endcan
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
