@extends('adminlte::page')
@section('title', __('Create New Meal').' - '.env('APP_NAME'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 pt-3">
                <div class="card">
                    <h5 class="card-header">{{__('Create New Meal')}}</h5>
                    <div class="card-body">
                        <form method="post" action="{{route('meals.store')}}"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="row mb-3">
                                <label for="food_product_id"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Meal Name') }}</label>
                                <div class="col-md-6">
                                    <input id="amount" type="text"
                                           class="form-control @error('name') is-invalid @enderror"
                                           name="name"
                                           value="{{ old('name') }}" required autocomplete="amount" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="recipe"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Recipes, included in the meal:') }}</label>
                                <div class="col-md-6">

                                    <ul style="list-style-type: none" class="recipes_selector">
                                        @foreach(\App\Models\Recipe::all() as $recipe)
                                            <li>
                                                <input type="checkbox" id="recipe" name="recipes[]" value="{{$recipe->id}}">
                                            {{$recipe->name}}
                                            <li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="ingredient"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Ingredients') }}</label>
                                <div class="col-md-6">

                                    <ul style="list-style-type: none" class="ingredients_selector">
                                        @foreach(\App\Models\ProductIngredient::all() as $ingredient)
                                            <li>
                                                <input type="checkbox" id="ingredient" name="ingredients[]" value="{{$ingredient->id}}">
                                            {{$ingredient->foodProduct->name. '-'.$ingredient->amount. ' ' .$ingredient->amount_type}}
                                            <li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="portion"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Supplement portion (optional):') }}</label>
                                <div class="col-md-6">

                                    <ul style="list-style-type: none" class="portions_selector">
                                        @foreach(\App\Models\SupplementPortion::all() as $portion)
                                            <li>
                                                <input type="checkbox" id="portion" name="portions[]" value="{{$portion->id}}">
                                            {{$portion->supplement->name. '-'.$portion->amount. ' ' .$portion->amount_type}}
                                            <li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="meal_calories"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Total Calories:') }}</label>
                                <div class="col-md-6">
                                    <input id="meal_calories" type="number"
                                           class="form-control @error('meal_calories') is-invalid @enderror"
                                           name="meal_calories"
                                           value="{{ old('meal_calories') }}" required autocomplete="meal_calories" autofocus>

                                    @error('meal_calories')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="notes"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Notes for the current meal:') }}</label>
                                <div class="col-md-6">
                                    <textarea cols="35" rows="5" id="notes" type="text"
                                              class="form-control @error('notes') is-invalid @enderror"
                                              name="notes"
                                              value="{{ old('notes') }}"  autocomplete="name"></textarea>
                                    @error('notes')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create New') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
