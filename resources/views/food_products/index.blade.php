@extends('adminlte::page')
@section('Title', __('Foods'))


@section('content')
    <div class="container">
        <h1 class="cart-header text-center">{{__('Create Food Product')}}</h1>
        <div class="mb-3 text-right">
            <a href="{{route('food_products.create')}}"
               class="btn btn-primary mb-3 mt-2 text-right">{{__('Create New')}}</a>
        </div>
        <div class="row justify-content-center">

            <table class="table table-dark">
                <thead>
                <th>{{__('ID')}}</th>
                <th>{{__('Name')}}</th>
                <th>{{__('Calories')}}</th>
                <th>{{__('Carbs')}}</th>
                <th>{{__('Protein')}}</th>
                <th>{{__('Fat')}}</th>
                <th>{{__('Category')}}</th>
                <th>{{__('Actions')}}</th>
                </thead>
                <tbody>
                @foreach($foodProducts as $foodProduct)
                    <tr>
                        <td>{{ucwords($foodProduct->id)}}</td>
                        <td>{{ucwords($foodProduct->name)}}</td>
                        <td>{{ucwords($foodProduct->calories)}}</td>
                        <td>{{ucwords($foodProduct->carbs)}}</td>
                        <td>{{ucwords($foodProduct->protein)}}</td>
                        <td>{{ucwords($foodProduct->fat)}}</td>
                        <td>{{($foodProduct->foodCategory->name ?? 'no data')}}</td>

                        <td>
                            <div class="d-flex">
                                <div class="edit mx-2">
                                    <a href="{{route('food_products.edit', $foodProduct->id)}}" class="btn btn-primary">{{__('Edit')}}</a>
                                </div>
                                <form  method="post" action="{{route('food_products.delete', $foodProduct->id)}}" class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger"
                                            onclick="return confirm('Are you sure you want to delete this item?');">{{__('DELETE')}}</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection
</body>
</html>

