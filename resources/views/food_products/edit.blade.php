@extends('adminlte::page')
@section('content')
    <div class="container">
        <div class="row justify-content-center mt-3">
            <div class="col-md-8 pt-3">
                <div class="card">
                    <h5 class="card-header text-center fw-bold">{{__('Edit Food Product')}}</h5>
                    <div class="card-body">
                        <form method="post" action="{{route('food_products.update', $foodProduct->id)}}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end fw-bold">
                                    {{__('Product Name')}}
                                </label>
                                <div class="col-md-6">
                                    <input type="text"
                                           name="name"
                                           class="form-control @error('name') is-invalid @enderror"
                                           value="{{$foodProduct->name}}" required>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="calories" class="col-md-4 col-form-label text-md-end fw-bold">
                                    {{__('Calories')}}
                                </label>
                                <div class="col-md-6">
                                    <input type="number"
                                           name="calories"
                                           class="form-control @error('calories') is-invalid @enderror"
                                           value="{{$foodProduct->calories}}" required>
                                    @error('calories')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="carbs" class="col-md-4 col-form-label text-md-end fw-bold">
                                    {{__('Carbs')}}
                                </label>
                                <div class="col-md-6">
                                    <input type="number"
                                           name="carbs"
                                           class="form-control  @error('carbs') is-invalid @enderror"
                                           value="{{$foodProduct->carbs}}" required>
                                    @error('carbs')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="protein" class="col-md-4 col-form-label text-md-end fw-bold">
                                    {{__('Protein')}}
                                </label>
                                <div class="col-md-6">
                                    <input type="number"
                                           name="protein"
                                           class="form-control  @error('protein') is-invalid @enderror"
                                           value="{{$foodProduct->protein}}" required>
                                    @error('protein')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="fat" class="col-md-4 col-form-label text-md-end fw-bold">
                                    {{__('Fat')}}
                                </label>
                                <div class="col-md-6">
                                    <input type="number"
                                           name="fat"
                                           class="form-control  @error('fat') is-invalid @enderror"
                                           value="{{$foodProduct->fat}}" required>
                                    @error('fat')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3 justify-content-center">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
