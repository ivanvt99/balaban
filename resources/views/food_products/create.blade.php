@extends('adminlte::page')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 pt-3">
                <div class="card">
                    <h5 class="card-header">{{ __('Create New Food Product') }}</h5>
                    <div class="card-body">
                        <form method="POST" action="{{route('food_products.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row mb-3">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-end">{{__('Name')}}</label>

                                <div class="col-md-6">
                                    <input type="text"
                                           name="name"
                                           id="name"
                                           class="form-control @error('name') is-invalid @enderror"
                                           value="{{ old('name') }}" required
                                           autocomplete="name" autofocus>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="food_category_id"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Food Category') }}</label>

                                <div class="col-md-6">
                                    <select id="food_category_id"
                                            class="form-control @error('food_category_id') is-invalid @enderror"
                                            name="food_category_id"
                                            required autofocus>
                                        <option selected disabled>{{__('Choose product category')}}...</option>
                                        @foreach(\App\Models\FoodCategory::all() as $category)
                                            <option value="{{$category->id}}">{{ ucwords($category->name) }}</option>
                                        @endforeach
                                    </select>

                                    @error('food_category_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="calories"
                                       class="col-md-4 col-form-label text-md-end">{{__('Calories')}}</label>

                                <div class="col-md-6">
                                    <input type="number"
                                           name="calories"
                                           id="calories"
                                           class="form-control @error('calories') is-invalid @enderror"
                                           value="{{ old('calories') }}" required
                                           autocomplete="calories" autofocus>
                                    @error('calories')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="carbs"
                                       class="col-md-4 col-form-label text-md-end">{{__('Carbs')}}</label>

                                <div class="col-md-6">
                                    <input type="number"
                                           name="carbs"
                                           id="carbs"
                                           class="form-control @error('carbs') is-invalid @enderror"
                                           value="{{ old('carbs') }}" required
                                           autocomplete="carbs" autofocus>
                                    @error('carbs')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="row mb-3">
                                <label for="protein"
                                       class="col-md-4 col-form-label text-md-end">{{__('Protein')}}</label>

                                <div class="col-md-6">
                                    <input type="number"
                                           name="protein"
                                           id="protein"
                                           class="form-control @error('protein') is-invalid @enderror"
                                           value="{{ old('protein') }}" required
                                           autocomplete="protein" autofocus>
                                    @error('protein')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="fat"
                                       class="col-md-4 col-form-label text-md-end">{{__('Fat')}}</label>

                                <div class="col-md-6">
                                    <input type="number"
                                           name="fat"
                                           id="fat"
                                           class="form-control @error('fat') is-invalid @enderror"
                                           value="{{ old('fat') }}" required
                                           autocomplete="fat" autofocus>
                                    @error('fat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create Product') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
