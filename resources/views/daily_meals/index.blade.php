@extends('adminlte::page')
@section('Title', __('Meal Days'))

@section('content')
    <div class="container">
        <h1 class="text-center mt-3">{{__('Meals')}}</h1>
        <div class="mb-3 mt-3 text-right">
            <a href="{{route('daily_meals.create')}}"
               class="btn btn-primary float-end">{{__('Create new')}}
            </a>
        </div>
        <div class="row justify-content-center">
            <table class="table table-dark">
                <thead>
                <th>{{__('ID')}}</th>
                <th>{{__('Day')}}</th>
                <th>{{__('Meals')}}</th>
                <th>{{__('Daily Calories')}}</th>
                <th>{{__('Target')}}</th>
                <th>{{__('Actions')}}</th>
                </thead>
                <tbody>
                @foreach( $dailyMeals as  $meal)
                    <tr>
                        <td>{{$meal->id}}</td>
                        <td>{{$meal->day_name}}</td>
                        <td>
                            <a href="{{route('daily_meals.show', $meal->id)}}">
                                {{( $meal->meals()->count())}}
                                @if($meal->meals()->count() > 1)
                                    meals
                                @else
                                    meal
                                @endif
                            </a>
                        </td>
                        <td>{{( $meal->daily_calories)}}</td>
                        <td>
                            @if($meal->target_calories)
                                {{( $meal->target_calories)}}
                            @else
                                {{__('No Specific Target for the day ')}}
                            @endif
                        </td>
                        <td>
                            <div class="d-flex">
                                <div class="edit mx-2">
                                    <a href="{{route('daily_meals.edit',  $meal->id)}}"
                                       class="btn btn-primary">{{__('Edit')}}</a>
                                </div>
                                <form method="post" action="{{route('daily_meals.delete',  $meal->id)}}"
                                      class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger"
                                            onclick="return confirm('Are you sure you want to delete this item?');">{{__('DELETE')}}</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
