@extends('adminlte::page')
@section('title', __('Create New Daily Meal').' - '.env('APP_NAME'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 pt-3">
                <div class="card">
                    <h5 class="card-header">{{__('Create New Daily Meal')}}</h5>
                    <div class="card-body">
                        <form method="post" action="{{route('daily_meals.update', $dailyMeal->id)}}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row mb-3">
                                <label for="day_name"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Day Name') }}</label>
                                <div class="col-md-6">
                                    <input id="day_name" type="text"
                                           class="form-control @error('name') is-invalid @enderror"
                                           name="day_name"
                                           value="{{ $dailyMeal->day_name }}" required autocomplete="amount" autofocus>

                                    @error('day_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="meal"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Recipes, included in the meal:') }}</label>
                                <div class="col-md-6">

                                    <ul style="list-style-type: none" class="meals_selector">
                                        @foreach(\App\Models\Meal::all() as $meal)
                                            <li>
                                                <input type="checkbox" id="meal" name="meals[]" value="{{$meal->id}}"
                                                       @if($dailyMeal->meals->where('id', $meal->id)->count()) checked @endif>
                                            {{$meal->name}}
                                            <li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="daily_calories"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Daily Calories:') }}</label>
                                <div class="col-md-6">
                                    <input id="daily_calories" type="number"
                                           class="form-control @error('daily_calories') is-invalid @enderror"
                                           name="daily_calories"
                                           value="{{ $dailyMeal->daily_calories }}" required
                                           autocomplete="daily_calories" autofocus>

                                    @error('daily_calories')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="target_calories"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Target Calories (optional):') }}</label>
                                <div class="col-md-6">
                                    <input id="target_calories" type="number"
                                           class="form-control @error('target_calories') is-invalid @enderror"
                                           name="target_calories"
                                           value="{{ $dailyMeal->target_calories }}" autocomplete="target_calories"
                                           autofocus>

                                    @error('target_calories')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
