@extends('adminlte::page')
@section('title', __('Create New Diet').' - '.env('APP_NAME'))
@section('js')
    <script>
        var loadFile = function (event) {
            var output = document.getElementById('image');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function () {
                URL.revokeObjectURL(output.src)
            }
        };
    </script>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 pt-3">
                <div class="card">
                    <h5 class="card-header">{{__('Edit Diet/')}}<b>{{$diet->name}}</b></h5>
                    <div class="card-body">
                        <form method="post" action="{{route('diets.update', $diet->id)}}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row mb-3">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Diet Name') }}</label>
                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror"
                                           name="name"
                                           value="{{ $diet->name }}" required autocomplete="amount" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="description"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Description') }}</label>
                                <div class="col-md-6">
                                    <textarea cols="35" rows="5" id="description" type="text"
                                              class="form-control @error('description') is-invalid @enderror"
                                              name="description"
                                              value="{{ old('description') }}" required autocomplete="name">{{$diet->description}}
                                    </textarea>
                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="time_range"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Time Range:') }}</label>
                                <div class="col-md-6">
                                    <input id="time_range" type="number"
                                           class="form-control @error('time_range') is-invalid @enderror"
                                           name="time_range"
                                           value="{{ $diet->time_range }}" required autocomplete="time_range" autofocus>

                                    @error('time_range')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="time_range_type"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Range Type') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control @error('time_range_type') is-invalid @enderror"
                                            name="time_range_type" required>
                                        <option value="days"
                                                @if($diet->time_range_type == 'days')selected @endif>{{__('Days')}}</option>
                                        <option value="weeks"
                                                @if($diet->time_range_type == 'weeks')selected @endif>{{__('Weeks')}}</option>
                                        <option value="mounts"
                                                @if($diet->time_range_type == 'mounts')selected @endif>{{__('Mounts')}}</option>
                                    </select>
                                    @error('time_range_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="daily_meal"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Meal Days:') }}</label>
                                <div class="col-md-6">

                                    <ul style="list-style-type: none" class="meals_selector">
                                        @foreach(\App\Models\DailyMeal::all() as $dailyMeal)
                                            <li>
                                                <input type="checkbox" id="daily_meal" name="daily_meals[]"
                                                       value="{{$dailyMeal->id}}"
                                                       @if($diet->dailyMeals->where('id', $dailyMeal->id)->count()) checked @endif>
                                            {{$dailyMeal->day_name}}
                                            <li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="client_id"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Assign To Client (optional):') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control @error('client_id') is-invalid @enderror"
                                            name="client_id">
                                        @if(\Illuminate\Support\Facades\Auth::user()->hasRole('trainer'))
                                            <option value="" selected disabled>{{__('Choose range type')}}...</option>
                                        @else
                                            @foreach(\App\Models\Client::all() as $client)
                                                <option value="{{$client->id}}"
                                                        @if($diet->client_id == $client->id) selected @endif>{{$client->client_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('client_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="trainer_id"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Made By:') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control @error('trainer_id') is-invalid @enderror"
                                            name="trainer_id" required>
                                        @foreach(\App\Models\Trainer::all() as $trainer)
                                            <option value="{{$trainer->id}}" @if($diet->trainer_id == $trainer->id) selected @endif
                                                    @if(auth()->user()->hasRole('trainer'))@if($trainer->id != auth()->user()->trainer->id) disabled @endif @endif>{{ $trainer->trainer_name }}</option>
                                        @endforeach
                                    </select>
                                    @error('trainer_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-md-4 col-form-label text-md-end">{{__('Image')}}</label>
                                <div class="col-sm-6">
                                    <input type="file" name="image" id="file" accept="image/*"
                                           class="form-control-file @error('image') is-invalid @enderror"
                                           onchange="loadFile(event)">
                                    @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                            <div class="row mb-0 justify-content-center">
                                <div id="images" class="col-12 text-center">
                                    <img id="image"
                                         @if($diet->getMedia('diet')->count() > 0) src="{{$diet->getMedia('diet')->first()->getUrl()}}"
                                         @endif style="width: 200px">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
