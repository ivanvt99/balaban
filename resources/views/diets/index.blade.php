@extends('adminlte::page')
@section('Title', __('test'))
@section('content')
    <div class="container">
        <h1 class="text-center mt-3">{{__('Diets')}}</h1>
        <div class="mb-3 mt-3 text-right">
            <a href="{{route('diets.create')}}"
               class="btn btn-primary float-end">{{__('Create new')}}
            </a>
        </div>
        <div class="row justify-content-center">
            <table class="table table-dark">
                <thead>
                <th>{{__('ID')}}</th>
                <th>{{__('Photo')}}</th>
                <th>{{__('Diet Name')}}</th>
                <th>{{__('Description')}}</th>
                <th>{{__('Diet Timeframe')}}</th>
                <th>{{__('Days variations')}}</th>
                <th>{{__('Published by:')}}</th>
                <th>{{__('Assigned to:')}}</th>
                <th>{{__('Actions')}}</th>
                </thead>
                <tbody>
                @foreach( $diets as  $diet)
                    <tr>
                        <td>{{$diet->id}}</td>
                        <td>
                            @if($diet->getMedia('diet')->count() > 0)
                                <img
                                    src="{{$diet->getMedia('diet')->first()->getUrl()}}"
                                    style="width: 100px;">
                            @else
                                {{__('No Data')}}
                            @endif
                        </td>
                        <td>{{$diet->name}}</td>
                        <td>{{$diet->description}}</td>
                        <td>{{$diet->time_range.' '.$diet->time_range_type }}</td>
                        <td>
                            <a href="{{route('diets.show', $diet->id)}}">
                                {{( $diet->dailyMeals()->count())}}
                                @if($diet->dailyMeals()->count() > 1)
                                    days
                                @else
                                    day
                                @endif
                            </a>
                        </td>
                      <td>{{$diet->trainer->trainer_name}}</td>
                      <td>
                          @if($diet->client_id)
                          {{$diet->client->client_name}}</td>
                        @else
                            {{__('The diet is public')}}
                        @endif
                        <td>
                            <div class="d-flex">
                                <div class="edit mx-2">
                                    <a href="{{route('diets.edit',  $diet->id)}}"
                                       class="btn btn-primary">{{__('Edit')}}</a>
                                </div>
                                <form method="post" action="{{route('diets.delete',  $diet->id)}}"
                                      class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger"
                                            onclick="return confirm('Are you sure you want to delete this item?');">{{__('DELETE')}}</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
