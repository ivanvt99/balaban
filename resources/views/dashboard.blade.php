@extends('adminlte::page')
@section('title', __('Dashboard').' - '.env('APP_NAME'))
@section('content')
    <div class="container">
        <h2 class="pt-3">Dashboard</h2>
        <div class="row justify-content-center">
            <div class="col-md-12 d-flex">
                <div class="mx-2 w-50">
                    <x-adminlte-small-box title="test" text="Trainers" icon="fas fa-dumbbell"
                                          theme="teal" url="{{route('trainers.index')}}" url-text="View details"
                                          id="test"/>
                </div>
                <div class="mx-2 w-50">
                    <x-adminlte-small-box title="Test" text="Food Products" icon="fas fa-carrot"
                                          theme="teal" url="{{route('food_products.index')}}" url-text="View details"/>
                </div>
                <div class="mx-2 w-50">
                    <x-adminlte-small-box title="Test" text="Users" icon="fas fa-user-tie"
                                          theme="teal" url="{{route('users.index')}}" url-text="View details"/>
                </div>
                <div class="mx-2 w-50">
                    <x-adminlte-small-box title="Test" text="Orders" icon="fas fa-user-tie"
                                          theme="teal" url="{{route('trainers.index')}}" url-text="View details"/>
                </div>
                <div class="mx-2 w-50">
                    <x-adminlte-small-box title="Test" text="Invoices" icon="fas fa-finance"
                                          theme="teal" url="{{route('trainers.index')}}" url-text="View details"/>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
