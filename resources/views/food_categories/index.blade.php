@extends('adminlte::page')
@section('Title', __('Food Categories'))

@section('content')
    <div class="container">
            <h1 class="text-center mt-3">{{__('Food Categories')}}</h1>
            <div class="mb-3 mt-3 text-right">
                <a href="{{route('food_categories.create')}}"
                   class="btn btn-primary float-end">{{__('Create new')}}
                </a>
            </div>
        <div class="row justify-content-center">
                <table class="table table-dark">
                    <thead>
                    <th>{{__('ID')}}</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('Slug')}}</th>
                    <th>{{__('Actions')}}</th>
                    </thead>
                    <tbody>
                    @foreach($foodCategories as $foodCategory)
                        <tr>
                            <td>{{($foodCategory->id)}}</td>
                            <td>{{($foodCategory->name)}}</td>
                            <td>{{($foodCategory->slug)}}</td>

                            <td>
                                <div class="d-flex">
                                    <div class="edit mx-2">
                                        <a href="{{route('food_categories.edit', $foodCategory->id)}}" class="btn btn-primary">{{__('Edit')}}</a>
                                    </div>
                                    <form  method="post" action="{{route('food_categories.delete', $foodCategory->id)}}" class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger"
                                                onclick="return confirm('Are you sure you want to delete this item?');">{{__('DELETE')}}</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
        </div>
    </div>
@endsection
