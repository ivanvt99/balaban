@extends('adminlte::page')
@section('Title', __('Muscle Groups'))

@section('content')
    <div class="container">
        <h1 class="text-center mt-3">{{__('Muscle Groups')}}</h1>
        <div class="mb-3 mt-3 text-right">
            <a href="{{route('muscle_groups.create')}}"
               class="btn btn-primary float-end">{{__('Create new')}}
            </a>
        </div>
        <div class="row justify-content-center">
            <table class="table table-dark">
                <thead>
                <th>{{__('ID')}}</th>
                <th>{{__('Name')}}</th>
                <th>{{__('Slug')}}</th>
                <th>{{__('Actions')}}</th>
                </thead>
                <tbody>
                @foreach($muscles as $muscle)
                    <tr>
                        <td>{{($muscle->id)}}</td>
                        <td>{{($muscle->name)}}</td>
                        <td>{{($muscle->slug)}}</td>

                        <td>
                            <div class="d-flex">
                                <div class="edit mx-2">
                                    <a href="{{route('muscle_groups.edit', $muscle->id)}}" class="btn btn-primary">{{__('Edit')}}</a>
                                </div>
                                <form  method="post" action="{{route('muscle_groups.delete', $muscle->id)}}" class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger"
                                            onclick="return confirm('Are you sure you want to delete this item?');">{{__('DELETE')}}</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
