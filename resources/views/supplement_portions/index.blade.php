@extends('adminlte::page')
@section('Title', __('Supplement Portions'))
@section('content')
    <div class="container">
        <h1 class="text-center mt-3">{{__('Supplement Portions')}}</h1>
        <div class="mb-3 mt-3 text-right">
            @can('supplement-portion-create')
                <a href="{{route('supplement_portions.create')}}"
                   class="btn btn-primary float-end">{{__('Create new')}}
                </a>
            @endcan
        </div>
        <div class="row justify-content-center">
            <table class="table table-dark">
                <thead>
                <th>{{__('ID')}}</th>
                <th>{{__('Supplement')}}</th>
                <th>{{__('Amount per portion')}}</th>
                <th>{{__('Amount_type')}}</th>
                <th>{{__('Actions')}}</th>
                </thead>
                <tbody>
                @foreach( $portions as  $portion)
                    <tr>
                        <td>{{$portion->id}}</td>
                        <td>{{( $portion->supplement->name)}}</td>
                        <td>{{( $portion->amount)}}</td>
                        <td>{{( $portion->amount_type)}}</td>
                        <td>
                            <div class="d-flex">
                                <div class="edit mx-2">
                                    @can('supplement-portion-edit')
                                        <a href="{{route('supplement_portions.edit',  $portion->id)}}"
                                           class="btn btn-primary">{{__('Edit')}}</a>
                                    @endcan
                                </div>
                                @can('supplement-portion-delete')
                                <form method="post" action="{{route('supplement_portions.delete',  $portion->id)}}"
                                      class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger"
                                            onclick="return confirm('Are you sure you want to delete this item?');">{{__('DELETE')}}</button>
                                </form>
                                @endcan
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
