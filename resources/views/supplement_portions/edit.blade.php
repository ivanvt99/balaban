@extends('adminlte::page')
@section('title', __('Create New Ingredient').' - '.env('APP_NAME'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 pt-3">
                <div class="card">
                    <h5 class="card-header">{{__('Create New Ingredient')}}</h5>
                    <div class="card-body">
                        <form method="post" action="{{route('supplement_portions.update', $portion->id)}}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row mb-3">
                                <label for="supplement_id"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Supplement') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control @error('supplement_id') is-invalid @enderror"
                                            name="supplement_id" required>
                                        <option value="{{$portion->supplement_id}}"
                                                selected>{{$portion->supplement->name}}</option>
                                        @foreach(\App\Models\Supplement::all() as $supplement)
                                            <option value="{{$supplement->id}}">{{$supplement->name}}</option>
                                        @endforeach
                                    </select>

                                    @error('supplement_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="amount"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Amount per portion') }}</label>
                                <div class="col-md-6">
                                    <input id="amount" type="number"
                                           class="form-control @error('amount') is-invalid @enderror"
                                           name="amount"
                                           value="{{ $portion->amount }}" required autocomplete="amount" autofocus>

                                    @error('amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="amount_type"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Amount Type') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control @error('amount_type') is-invalid @enderror"
                                            name="amount_type" required>
                                        <option value="" selected disabled>{{__('Choose amount type')}}...</option>
                                        <option value="grams"
                                                @if($portion->amount_type == 'grams')selected @endif>{{__('Grams')}}</option>
                                        <option value="ml"
                                                @if($portion->amount_type == 'ml')selected @endif>{{__('Ml/s')}}</option>
                                        <option value="liter"
                                                @if($portion->amount_type == 'liter')selected @endif>{{__('Liter/s')}}</option>
                                        <option value="oz"
                                                @if($portion->amount_type == 'oz')selected @endif>{{__('Ounce/s')}}</option>
                                        <option value="piece"
                                                @if($portion->amount_type == 'piece')selected @endif>{{__('Piece/s')}}</option>
                                        <option value="scoop"
                                                @if($portion->amount_type == 'scoop')selected @endif>{{__('Scoop/s')}}</option>
                                        <option value="pill"
                                                @if($portion->amount_type == 'pill')selected @endif>{{__('Pill/s')}}</option>
                                    </select>
                                    @error('amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
