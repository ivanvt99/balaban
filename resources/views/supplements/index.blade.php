@extends('adminlte::page')

@section('title', __('Product Option Values').' - '.env('APP_NAME'))

@section('content')
    <h1 class="text-center">{{__('Supplements')}}</h1>
    <div class="row justify-content-center">
        <div class="col-12">
            @can('supplement-create')
                <a href="{{route('supplements.create')}}"
                   class="btn btn-primary mb-3 float-right">{{__('Create New')}}</a>
            @endcan
            <table class="table table-dark">
                <tr>
                    <th>{{__('Photo')}}</th>
                    <th>{{__('ID')}}</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('Description')}}</th>
                    <th>{{__('Slug')}}</th>
                    <th>{{__('Actions')}}</th>
                </tr>
                @foreach($supplements as $supplement)

                    <tr>
                        <td>
                            @if($supplement->getMedia('supplement')->count() > 0)
                                <img
                                    src="{{$supplement->getMedia('supplement')->first()->getUrl()}}"
                                    style="width: 100px;">
                                </a>
                            @else
                                {{__('No Data')}}
                            @endif
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            @if($supplement->id)
                                {{$supplement->id}}
                            @else
                                {{__('No Data')}}

                            @endif
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            @if($supplement->name)
                                {{$supplement->name}}
                            @else
                                {{__('No Data')}}

                            @endif
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            @if($supplement->description)
                                {{$supplement->description}}
                            @else
                                {{__('No Data')}}

                            @endif
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            @if($supplement->slug)
                                {{$supplement->slug}}
                            @else
                                {{__('No Data')}}

                            @endif
                        </td>
                        <td class="d-flex" style="text-align: center; vertical-align: middle;">
                            @can('supplement-edit')
                                <a href="{{route('supplements.edit',$supplement->id)}}"
                                   class="text-decoration-none text-white mx-2">
                                    <button class="btn btn-primary">{{__('Edit')}}</button>
                                </a>
                            @endcan
                            @can('supplement-delete')
                                <form action="{{route('supplements.delete', $supplement->id)}}"
                                      method="post" class="d-inline" autocomplete="off">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit"
                                            onclick="return confirm('Are you sure you want to delete this item?');">
                                        {{__('Delete')}}
                                    </button>
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    {{--    {{ $supplements->links('pagination::bootstrap-5') }}--}}
@endsection
