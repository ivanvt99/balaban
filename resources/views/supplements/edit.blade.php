@extends('adminlte::page')

@section('title', __('Create New Recipe'))
@section('js')
    <script>
        var loadFile = function (event) {
            var output = document.getElementById('image');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function () {
                URL.revokeObjectURL(output.src)
            }
        };
    </script>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 pt-3">
                <div class="card">

                    <h5 class="card-header">{{ __('Edit Supplement') }}</h5>

                    <div class="card-body">
                        <form method="post" action="{{route('supplements.update', $supplement->id)}}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row mb-3">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>
                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror"
                                           name="name"
                                           value="{{ $supplement->name }}" required autocomplete="name">

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="description"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Description') }}</label>
                                <div class="col-md-6">
                                    <textarea cols="35" rows="5" id="description" type="text"
                                              class="form-control @error('description') is-invalid @enderror"
                                              name="description"
                                              value="{{ old('description') }}" required autocomplete="name">{{$supplement->description}}</textarea>
                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-md-4 col-form-label text-md-end">{{__('Image')}}</label>
                                <div class="col-sm-6">
                                    <input type="file" name="image"
                                           class="form-control @error('image') is-invalid @enderror"
                                           onchange="loadFile(event)">
                                    @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Edit') }}
                                    </button>
                                </div>
                            </div>
                            <div class="row mb-0 justify-content-center">
                                <div id="images" class="col-12 text-center">
                                    <img id="image"   @if($supplement->getMedia('supplement')->count() > 0) src="{{$supplement->getMedia('supplement')->first()->getUrl()}}" @endif style="width: 200px">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
