@extends('adminlte::page')

@section('title', __('Edit Client').' - '.env('APP_NAME'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 pt-3">
                <div class="card">
                    <h5 class="card-header">{{ __('Edit Client') }}</h5>
                    <div class="card-body">
                                <form method="post" action="{{route('clients.update', $client->id)}}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row mb-3">
                                        <label for="name"  @if(auth()->user()->hasRole('trainer')) style="display: none" @endif
                                        class="col-md-4 col-form-label text-md-end">{{ __('Trainer') }}</label>
                                        <div class="col-md-6">
                                            <select class="form-control @error('trainer_id') is-invalid @enderror"
                                                    name="trainer_id" @if(auth()->user()->hasRole('dealer')) style="display:none" @endif required>
                                                <option value="" disabled selected>Choose a dealer for the client...
                                                </option>
                                                @foreach(\App\Models\Trainer::all() as $trainer)
                                                    <option value="{{$trainer->id}}"
                                                            @if($trainer->id == $client->trainer->id) selected="selected"
                                                            @endif @if(auth()->user()->hasRole('trainer'))@if($trainer->id != auth()->user()->trainer->id) disabled @endif @endif>{{ $trainer->trainer_name }}</option>
                                                @endforeach
                                            </select>

                                            @error('trainer_id')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="status"
                                               class="col-md-4 col-form-label text-md-end">{{ __('Status') }}</label>
                                        <div class="col-md-6">
                                            <select class="form-control @error('status') is-invalid @enderror" name="status"
                                                    required>
                                                <option value="active"
                                                        @if($client->status == 'active') selected @endif>{{__('Active')}}</option>
                                                <option value="inactive"
                                                        @if($client->status == 'inactive') selected @endif>{{__('Inactive')}}</option>
                                                <option value="suspended"
                                                        @if($client->status == 'suspended') selected @endif>{{__('Suspended')}}</option>
                                            </select>

                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary mt-3">{{__('Save')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
