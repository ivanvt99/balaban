@extends('adminlte::page')

@section('title', __('Clients').' - '.env('APP_NAME'))

@section('content')
    <div class="row justify-content-center">
        <h1 class="text-center">{{__('Clients')}}</h1>
        <div class="col-12">
            @if(session('error'))
                <div class="row justify-content-center">
                    <div class="alert alert-danger"> {{session('error')}}</div>
                </div>
            @endif
            @if(session('success'))
                <div class="row justify-content-center">
                    <div class="alert alert-success"> {{session('success')}}</div>
                </div>
            @endif
            @can('client-create')
                <a href="{{route('clients.create')}}" class="btn btn-primary mb-3 float-right">{{__('Create New')}}</a>
            @endcan
            <table class="table table-dark">
                <thead>
                <tr>
                    <th> {{__('ID')}}</th>
                    <th>{{__('Client Name')}}</th>
                    <th>{{__('Trainer')}}</th>
                    <th>{{__('Client User Email')}}</th>
                    @can('client-set-status')
                        <th>{{__('Status')}}</th>
                    @endcan
                    @if(Auth::user()->can('client-edit') || Auth::user()->can('client-delete'))
                        <th>{{__('Actions')}}</th>
                    @endcan
                </tr>
                </thead>
                <tbody>
                @foreach($clients as $client)
                    <tr>
                        <td>
                            @if($client->id)
                            {{$client->id}}
                            @else
                                {{__('No Data')}}
                            @endif
                        </td>
                        <td>
                            @if($client->client_name)
                                {{$client->client_name}}
                            @else
                                {{__('No Data')}}
                            @endif
                        </td>
                        <td>
                            @if($client->trainer->trainer_name)
                            {{$client->trainer->trainer_name}}
                            @else
                                {{__('No Data')}}
                            @endif
                        </td>
                        <td>
                            @if($client->user->email)
                            {{$client->user->email}}
                            @else
                                {{__('No Data')}}
                            @endif
                        </td>
                        @can('client-set-status')
                            <td>
                                @if($client->status)
                                {{$client->status}}
                                @else
                                    {{__('No Data')}}
                                @endif
                            </td>
                        @endcan
                        @if(Auth::user()->can('client-edit') || Auth::user()->can('client-delete'))
                            <td>
                                @can('client-edit')
                                    <a href="{{route('clients.edit', $client->id)}}"
                                       class="text-decoration-none text-white">
                                        <button class="btn btn-primary">{{__('Edit')}}</button>
                                    </a>
                                @endcan
                                @can('client-delete')
                                    <form action="{{route('clients.delete', $client->id)}}"
                                          method="post" class="d-inline" autocomplete="off">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger" type="submit"
                                                onclick="return confirm('Are you sure you want to delete this client?');">
                                            {{__('Delete')}}
                                        </button>
                                    </form>
                                @endcan
                            </td>
                        @endcan
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection


