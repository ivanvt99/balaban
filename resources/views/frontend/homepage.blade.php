@extends('layouts/app')
@section('content')
    {{--    <h1>hello</h1>--}}
    {{--    @if(\Illuminate\Support\Facades\Auth::user())--}}
    {{--        will be finished...--}}
    {{--    @endif--}}

    <div class="container-fluid pl-0 pr-0 bg-img clearfix parallax-window2" data-parallax="scroll"
         style="background-image: url('images/banner2.jpg') ">
        <div class="container">
            <div class="fh5co-banner-text-box">
                <div class="quote-box pl-5 pr-5 wow bounceInRight">
                    <h2> Find your passion <br><span>Become part of  us</span></h2>
                </div>
                {{--            <a href="#" class="btn text-uppercase btn-outline-danger btn-lg mr-3 mb-3 wow bounceInUp"> What's new</a> --}}
                {{--            <a href="#" class="btn text-uppercase btn-outline-danger btn-lg mb-3 wow bounceInDown"> Courses</a> --}}
            </div>
        </div>
    </div>
    <div class="container-fluid fh5co-network">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="wow bounceInUp">FOR OUR</h4>
                    <h2 class="wow bounceInRight">MEMBERS</h2>
                    <hr/>
                    <h5 class="wow bounceInLeft">RESHAPE SUMMER 2022</h5>
                    <p class="wow bounceInDown">The lowdown on Blood Flow Restriction training • FREE CEC/CPD • ​Let's
                        compare ​Raw Vs Cooked • Building Ab, Core &amp; Pelvic strength • Get​ clients short-term wins
                        from long-term goals • So you want to operate multiple clubs? + LOADS MORE</p>
                </div>
                <div class="col-md-6">
                    <figure class="wow bounceInDown"><img src="images/about-img.jpg" alt="gym" class="img-fluid"/>
                    </figure>
                </div>
            </div>
        </div>
    </div>
    <div id="about-us" class="container-fluid fh5co-about-us pl-0 pr-0 parallax-window" data-parallax="scroll"
         data-image-src="images/about-us-bg.jpg">
        <div class="container">
            <div class="col-sm-6 offset-sm-6">
                <h2 class="wow bounceInLeft" data-wow-delay=".25s">ABOUT US</h2>
                <hr/>
                <p class="wow bounceInRight" data-wow-delay=".25s">Trainers, athletes and serious clients alike are
                    looking for the toughest, most brutally productive training techniques to spice up their workouts
                    and provide a truly unique challenge for their body and mind. Think: one-arm push-ups, pistols,
                    pull-ups, handstands, hanging levers and the like! If you want together for </p>
                <a class="btn btn-lg btn-outline-danger d-inline-block text-center mx-auto wow bounceInDown"
                   href="{{url('/about_us')}}">Learn More</a></div>
        </div>
    </div>
    <div class="container-fluid fh5co-content-box">
        <div class="container">
            <div class="row">
                <div class="col-md-5 pr-0"><img src="images/rode-gym.jpg" alt="gym" class="img-fluid wow bounceInLeft"/>
                </div>
                <div class="col-md-7 pl-0">
                    <div class="wow bounceInRight" data-wow-delay=".25s">
                        <div class="card-img-overlay">
                            <p>Get in shape with a custom made diet, explore new ways to sculpt your body and implement
                                a routine that fits you well. Work smart, train hard and start achieving your goals
                                NOW! </p>
                        </div>
                        <img src="images/gym-girls.jpg" alt="girls in gym" class="img-fluid"/></div>
                </div>
            </div>
            <div class="row trainers pl-0 pr-0">
                <div class="col-12 bg-50">
                    <div class="quote-box2 wow bounceInDown" data-wow-delay=".25s">
                        <h2> TRAINERS </h2>
                    </div>
                </div>
                <div class="col-md-6 pr-5 pl-5">
                    <div class="card text-center wow bounceInLeft" data-wow-delay=".25s"><img
                            class="card-img-top rounded-circle img-fluid" src="images/trainers1.jpg" alt="Card image">
                        <div class="card-body mb-5">
                            <h4 class="card-title">JENIFERR</h4>
                            <p class="card-text">Trainers, athletes and serious clients alike are looking for the
                                toughest, most brutally productive training techniques to spice up their workouts and
                                provide a truly unique challenge</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 pl-5 pr-5">
                    <div class="card text-center wow bounceInRight" data-wow-delay=".25s"><img
                            class="card-img-top rounded-circle img-fluid" src="images/trainers2.jpg" alt="Card image">
                        <div class="card-body mb-5">
                            <h4 class="card-title">JHON</h4>
                            <p class="card-text">Trainers, athletes and serious clients alike are looking for the
                                toughest, most brutally productive training techniques to spice up their workouts and
                                provide a truly unique challenge</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gallery">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body mb-4 wow bounceInLeft" data-wow-delay=".25s">
                            <h4 class="card-title">FILEX</h4>
                            <p class="card-text">I just wanted to sincerely thank you for the opportunity to represent
                                Precision Nutrition and the US at such an amazing event. </p>
                        </div>
                        <img class="card-img-top img-fluid wow bounceInRight" data-wow-delay=".25s" src="images/g1.jpg"
                             alt="Card image"></div>
                </div>
                <div class="col-md-4">
                    <div class="card"><img class="card-img-top img-fluid wow bounceInUp" data-wow-delay=".25s"
                                           src="images/g2.jpg" alt="Card image">
                        <div class="card-body mt-4 wow bounceInDown" data-wow-delay=".25s">
                            <h4 class="card-title">IGNITING</h4>
                            <p class="card-text">I just wanted to sincerely thank you for the opportunity to represent
                                Precision Nutrition and the US at such an amazing event. </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body mb-4 wow bounceInRight" data-wow-delay=".25s">
                            <h4 class="card-title">PASSION</h4>
                            <p class="card-text">I just wanted to sincerely thank you for the opportunity to represent
                                Precision Nutrition and the US at such an amazing event. </p>
                        </div>
                        <img class="card-img-top img-fluid wow bounceInLeft" data-wow-delay=".25s" src="images/g3.jpg"
                             alt="Card image"></div>
                </div>
            </div>
        </div>
    </div>
    <footer class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-3 footer1 d-flex wow bounceInLeft" data-wow-delay=".25s">
                    <div class="d-flex flex-wrap align-content-center"><a href="#">Olympus Gym</a>
                        <p>FOUNDRY FOR CHAMPIONS</p>
                        <p>&copy; 2022 Olympus Gym. All rights reserved.</p>
                    </div>
                </div>
                <div class="col-md-6 footer2 wow bounceInUp" data-wow-delay=".25s" id="contact">
                    <div class="form-box">
                        <h4>CONTACT US</h4>
                        <form method="POST" action="{{route('inquiries.store')}}" enctype="multipart/form-data">
                            @csrf
                            <table class="table table-responsive d-table">
                                <tr>
                                    <td>
                                        <input id="title" type="text"
                                               class="form-control @error('title') is-invalid @enderror"
                                               name="title" value="{{ old('title') }}" placeholder="TITLE" required
                                               autocomplete="name" autofocus>
                                        @error('title')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </td>
                                    <td>
                                        <input id="sender_name" type="text"
                                               class="form-control @error('sender_name') is-invalid @enderror"
                                               name="sender_name" value="{{ old('sender_name') }}" required
                                               placeholder="SENDER NAME" autocomplete="name" autofocus>
                                        @error('sender_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input id="sender_phone_number" type="text"
                                               class="form-control @error('title') is-invalid @enderror"
                                               name="sender_phone_number" value="{{ old('sender_phone_number') }}"
                                               required
                                               placeholder="PHONE NUMBER" autocomplete="name" autofocus>
                                        @error('sender_phone_number')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input id="sender_email" type="email"
                                               class="form-control @error('sender_email') is-invalid @enderror"
                                               name="sender_email" value="{{ old('sender_email') }}" required
                                               placeholder="EMAIL" autocomplete="name" autofocus>
                                        @error('sender_email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        {{--status is hidden-> always status:pending on submit--}}
                                        <input type="hidden"
                                               id="status"
                                               name="status"
                                               value="pending">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                  <textarea
                                      class="form-control @error('content') is-invalid @enderror"
                                      name="content" required
                                      placeholder="Content" autocomplete="name" autofocus></textarea>

                                        @error('content')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="text-center pl-0">
                                        <button type="submit" class="btn btn-dark">SEND</button>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
                <div class="col-md-3 footer3 wow bounceInRight" data-wow-delay=".25s">
                    <h5>ADDRESS</h5>
                    <p>5100, Gorna Oryahovitsa Stojan Mihailovksi str. 29 ,BULGARIA</p>
                    <h5>PHONE</h5>
                    <p>253232323232</p>
                    <h5>EMAIL</h5>
                    <p>g.michaels@olympus.com</p>
                    <div class="social-links">
                        <ul class="nav nav-item">
                            <li><a href="https://m.facebook.com/profile.php?id=496016517116355" class="btn btn-secondary mr-1 mb-2"><img
                                        src="images/facebook.png" alt="facebook"/></a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Bootstrap JS, ... -->

    {{--<script src="js/jquery.min.js"></script>--}}
    <script src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/parallax.js"></script>
    <script src="js/wow.js"></script>
    <script src="js/main.js"></script>

@endsection
