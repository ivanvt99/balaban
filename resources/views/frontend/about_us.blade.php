@extends('layouts/app')
@section('content')
    <div class="test">
        <div class="mt-0 pl-0 pr-0 bg-img clearfix parallax-window2" data-parallax="scroll"
             style="background-image: url('images/banner2.jpg') ">
            <div class="fh5co-banner-text-box">
                <div class="quote-box pl-5 pr-5 wow bounceInRight">
                    <h2> Find out about us <br><span>Get in touch with out team</span></h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6 p-4" style="background: white">
                <div class="d-flex">
                    <div class="mt-2">
                        <img src="{{asset('images/gym2.jpg')}}" alt="contact-img" width="100%" height="100%">
                    </div>
                    <div>
                        <div class="quote-box pl-5 pr-5 wow bounceInRight">
                            <h2 >Foundry for champions </h2>
                            <p>Trainers, athletes and serious clients alike are looking for the toughest, most brutally
                                productive training techniques to spice up their workouts and provide a truly unique
                                challenge. Our team provides our customers with good quality steel for building their
                                iron body and will.</p>
                        </div>

                    </div>
                </div>
               <div class="contacts">
                   <h3 class="contact-heading text-center">Contact us:</h3>
                   George Michaels
                   <br>
                   <a href="tel: (+359) 885 55 66 88"> <i class="fa-solid fa-phone"></i>(+359) 885 55 66 88</a>
                   <br>
                   <i class="fa-solid fa-envelope"></i> g.michaels@olympus.com
               </div>
            </div>
            <div class="col-lg-6">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2912.0228154709384!2d25.67564032849928!3d43.12504691243162!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9e41a4a9c8eec91!2zNDPCsDA3JzMwLjIiTiAyNcKwNDAnNDguNSJF!5e0!3m2!1sbg!2sbg!4v1656347498189!5m2!1sbg!2sbg"
                    width="100%%" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                    referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
        <footer class="container-fluid mt-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 footer1 d-flex wow bounceInLeft" data-wow-delay=".25s">
                        <div class="d-flex flex-wrap align-content-center"><a href="#">Olympus Gym</a>
                            <p>FOUNDRY FOR CHAMPIONS</p>
                            <p>&copy; 2022 Olympus Gym. All rights reserved.</p>
                        </div>
                    </div>
                    <div class="col-md-6 footer2 wow bounceInUp" data-wow-delay=".25s" id="contact">
                        <div class="form-box">
                            <h4>CONTACT US</h4>
                            <form method="POST" action="{{route('inquiries.store')}}" enctype="multipart/form-data">
                                @csrf
                                <table class="table table-responsive d-table">
                                    <tr>
                                        <td>
                                            <input id="title" type="text"
                                                   class="form-control @error('title') is-invalid @enderror"
                                                   name="title" value="{{ old('title') }}" placeholder="TITLE" required
                                                   autocomplete="name" autofocus>
                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </td>
                                        <td>
                                            <input id="sender_name" type="text"
                                                   class="form-control @error('sender_name') is-invalid @enderror"
                                                   name="sender_name" value="{{ old('sender_name') }}" required
                                                   placeholder="SENDER NAME" autocomplete="name" autofocus>
                                            @error('sender_name')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <input id="sender_phone_number" type="text"
                                                   class="form-control @error('title') is-invalid @enderror"
                                                   name="sender_phone_number" value="{{ old('sender_phone_number') }}"
                                                   required
                                                   placeholder="PHONE NUMBER" autocomplete="name" autofocus>
                                            @error('sender_phone_number')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <input id="sender_email" type="email"
                                                   class="form-control @error('sender_email') is-invalid @enderror"
                                                   name="sender_email" value="{{ old('sender_email') }}" required
                                                   placeholder="EMAIL" autocomplete="name" autofocus>
                                            @error('sender_email')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            {{--status is hidden-> always status:pending on submit--}}
                                            <input type="hidden"
                                                   id="status"
                                                   name="status"
                                                   value="pending">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                  <textarea
                                      class="form-control @error('content') is-invalid @enderror"
                                      name="content" required
                                      placeholder="Content" autocomplete="name" autofocus></textarea>

                                            @error('content')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-center pl-0">
                                            <button type="submit" class="btn btn-dark">SEND</button>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3 footer3 wow bounceInRight" data-wow-delay=".25s">
                        <h5>ADDRESS</h5>
                        <p>5100, Gorna Oryahovitsa Stojan Mihailovksi str. 29 ,BULGARIA</p>
                        <h5>PHONE</h5>
                        <p>253232323232</p>
                        <h5>EMAIL</h5>
                        <p>g.michaels@olympus.com</p>
                        <div class="social-links">
                            <ul class="nav nav-item">
                                <li><a href="https://m.facebook.com/profile.php?id=496016517116355" class="btn btn-secondary mr-1 mb-2"><img
                                            src="images/facebook.png" alt="facebook"/></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

@endsection
