@extends('layouts/app')
@section('content')
    <h1 class="text-white text-center mb-3">Meals for the day:</h1>
    <div class="col-12">
        <div class="products d-flex flex-wrap align-content-center justify-content-center">
            @foreach( $meals  as $meal)
                <div class="col-12 col-xl-3 col-lg-6 col-md-6 ">
                    <div
                        class="item p-2 pb-3 bg-light d-flex flex-column justify-content-start overflow-hidden"
                        data-mh="ltx-wc-item">

                        <div class="recipe-title text-center"> {{ucwords($meal->name)}}</div>
                        <div class="post_content entry-content text-center">{{$meal->meal_calories}} calories
                        </div>
                        <div class="post_content entry-content text-start">
                            <h4>Recipes:</h4>
                            @foreach($meal->recipes as $recipe)
                                <a href="{{route('nutrition.showRecipe', $recipe->id)}}"><i
                                        class="fas fa-concierge-bell">{{$recipe->name}}</i></a>
                                <br>
                            @endforeach
                        </div>
                        <div class="post_content entry-content text-start">
                            <h4>Additional snacks:</h4>
                            @foreach($meal->productIngredients as $ingredient)
                                <i class="fas fa-apple-alt">{{$ingredient->foodProduct->name.'-'.$ingredient->amount.' '.$ingredient->amount_type }}</i>
                                <br>
                            @endforeach
                            @if($meal->productIngredients()->count() == 0)
                                {{__('No additions')}}
                            @endif
                        </div>
                        <div class="post_content entry-content text-start">
                            <h4>Supplements with the meal:</h4>
                            @foreach($meal->supplementPortions as $portion)

                                <i class="fas fa-pills">{{$portion->supplement->name.'-'.$portion->amount.' '.$portion->amount_type }}</i>
                                <br>
                            @endforeach
                            @if($meal->supplementPortions->count() == 0)
                                <i class="fas fa-pills">{{__('No Supplements')}}</i>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
