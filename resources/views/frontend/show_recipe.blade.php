@extends('layouts/app')
@section('content')
    <div class="container" style="text-align: -webkit-center;">
        <div class="row d-flex recipe-details">
            <div class="product-images-wrapper  col-12 col-xl-7 col-lg-5 col-md-12 mt-5 mb-2">
                <a href="{{$recipe->getMedia('recipe/'.$recipe->id)->first()->getUrl()}}"
                   data-lightbox="image" class="text-decoration-none">
                    <img
                        src="{{$recipe->getMedia('recipe/'.$recipe->id)->first()->getUrl()}}"
                        class="card-img-top" id="recipe_image"

                        alt="{{__('Product Image')}}">
                </a>
            </div>
            <div class="col-12 col-xl-5 col-lg-7 col-md-12 recipe-info mt-5">
                <h2 class="text-bold"><b>{{$recipe->name}}</b></h2>
                <div class="description-name">
                    <h3  class="description-title text-start"><i class="fa-solid fa-mortar-pestle"> Ingredients</i></h3>
                </div>
                <div class="description text-start">
                    @foreach($recipe->productIngredients as $ingredient)
                        {{$ingredient->foodProduct->name.' - '.
                        $ingredient->amount. ' '. $ingredient->amount_type}}
                        <br>
                    @endforeach
                </div>
                <div class="description-name">
                    <h3 class="description-title text-start"><i class="fa-solid fa-stopwatch"> Preparation Time:</i></h3>
                </div>
                <div class="description text-start" >
                    <p>  {{$recipe->preparation_time.' '.$recipe->time_amount }}</p>
                </div>
                <div class="description-name">
                    <h3 class="description-title text-start"><i class="fa-solid fa-stopwatch"> Cooking Time:</i></h3>
                </div>
                <div class="description text-start">
                    <p>  {{$recipe->cooking_time.' '.$recipe->time_amount }}</p>
                </div>
                <div class="description-name">
                    <h3 class="description-title text-start"><i class="fa-solid fa-wheat-awn-circle-exclamation">Calories per meal (400g):</i></h3>
                </div>
                <div class="description text-start">
                    <p>{{$recipe->calories}} calories</p>
                </div>
                <div class="description-name">
                    <h3 class="description-title text-start" ><i class="fa-solid fa-kitchen-set">How to cook it:</i></h3>
                </div>
                <div class="description text-start">
                    <p>{{$recipe->description}}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
