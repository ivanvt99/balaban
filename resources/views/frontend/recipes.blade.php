@extends('layouts/app')
@section('content')

    <div class="products d-flex flex-wrap align-content-center justify-content-left">
        @foreach($recipes as $recipe)
            <div class="col-12 col-xl-3 col-lg-6 col-md-6 ">
                <div class="item p-2 pb-3 bg-light d-flex flex-column justify-content-center overflow-hidden"
                     data-mh="ltx-wc-item">
                    <div class="image product-img overflow-hidden">
                        {{--                                <span class="onsale">Sale</span>--}}
                        <a href="{{$recipe->getMedia('recipe/'.$recipe->id)->first()->getUrl()}}"
                           data-lightbox="image-{{$recipe->id}}">
                            <img
                                src="{{$recipe->getMedia('recipe/'.$recipe->id)->first()->getUrl()}}"
                                style="width: 100%" class="zoom"
                                onerror="this.onerror=null; this.src='/images/default-image.png'">
                        </a>
                    </div>

                        <div class="recipe-title"> {{ucwords($recipe->name)}}</div>

                    <div
                        class="post_content entry-content">{{ucwords(\Str::limit($recipe->description,100))}}
                    </div>
                    <span class="price"></span>
                    <span
                        class="ltx-btn-wrap ltx-btn-wrap-main ltx-btn-wrap-hover-black d-flex justify-content-center">
                                <a
                                    href="{{route('nutrition.showRecipe', $recipe->id)}}"
                                    class="btn btn-primary show-recipe-button  btn btn-main  btn-xs"
                                    rel="nofollow">View more
                                </a>
                    </span>
                </div>
            </div>
        @endforeach
    </div>
    <footer class="container-fluid mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-3 footer1 d-flex wow bounceInLeft" data-wow-delay=".25s">
                    <div class="d-flex flex-wrap align-content-center"><a href="#">Olympus Gym</a>
                        <p>FOUNDRY FOR CHAMPIONS</p>
                        <p>&copy; 2022 Olympus Gym. All rights reserved.</p>
                    </div>
                </div>
                <div class="col-md-6 footer2 wow bounceInUp" data-wow-delay=".25s" id="contact">
                    <div class="form-box">
                        <h4>CONTACT US</h4>
                        <form method="POST" action="{{route('inquiries.store')}}" enctype="multipart/form-data">
                            @csrf
                            <table class="table table-responsive d-table">
                                <tr>
                                    <td>
                                        <input id="title" type="text"
                                               class="form-control @error('title') is-invalid @enderror"
                                               name="title" value="{{ old('title') }}" placeholder="TITLE" required
                                               autocomplete="name" autofocus>
                                        @error('title')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </td>
                                    <td>
                                        <input id="sender_name" type="text"
                                               class="form-control @error('sender_name') is-invalid @enderror"
                                               name="sender_name" value="{{ old('sender_name') }}" required
                                               placeholder="SENDER NAME" autocomplete="name" autofocus>
                                        @error('sender_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input id="sender_phone_number" type="text"
                                               class="form-control @error('title') is-invalid @enderror"
                                               name="sender_phone_number" value="{{ old('sender_phone_number') }}"
                                               required
                                               placeholder="PHONE NUMBER" autocomplete="name" autofocus>
                                        @error('sender_phone_number')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input id="sender_email" type="email"
                                               class="form-control @error('sender_email') is-invalid @enderror"
                                               name="sender_email" value="{{ old('sender_email') }}" required
                                               placeholder="EMAIL" autocomplete="name" autofocus>
                                        @error('sender_email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        {{--status is hidden-> always status:pending on submit--}}
                                        <input type="hidden"
                                               id="status"
                                               name="status"
                                               value="pending">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                  <textarea
                                      class="form-control @error('content') is-invalid @enderror"
                                      name="content" required
                                      placeholder="Content" autocomplete="name" autofocus></textarea>

                                        @error('content')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="text-center pl-0">
                                        <button type="submit" class="btn btn-dark">SEND</button>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
                <div class="col-md-3 footer3 wow bounceInRight" data-wow-delay=".25s">
                    <h5>ADDRESS</h5>
                    <p>5100, Gorna Oryahovitsa Stojan Mihailovksi str. 29 ,BULGARIA</p>
                    <h5>PHONE</h5>
                    <p>253232323232</p>
                    <h5>EMAIL</h5>
                    <p>g.michaels@olympus.com</p>
                    <div class="social-links">
                        <ul class="nav nav-item">
                            <li><a href="https://m.facebook.com/profile.php?id=496016517116355" class="btn btn-secondary mr-1 mb-2"><img
                                        src="images/facebook.png" alt="facebook"/></a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection
