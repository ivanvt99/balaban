@extends('adminlte::page')
@section('title', __('Edit New Ingredient').' - '.env('APP_NAME'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 pt-3">
                <div class="card">
                    <h5 class="card-header">{{__('Edit Ingredient')}}</h5>
                    <div class="card-body">
                        <form method="post" action="{{route('product_ingredients.update', $ingredient->id)}}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row mb-3">
                                <label for="food_product_id"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control @error('food_product_id') is-invalid @enderror"
                                            name="food_product_id" required>
                                        <option value="{{$ingredient->food_product_id}}"
                                                selected>{{$ingredient->foodProduct->name}}</option>
                                        @foreach(\App\Models\FoodProduct::all() as $product)
                                            <option value="{{$product->id}}">{{$product->name}}</option>
                                        @endforeach
                                    </select>

                                    @error('food_product_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="amount"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Amount') }}</label>
                                <div class="col-md-6">
                                    <input id="amount" type="number"
                                           class="form-control @error('amount') is-invalid @enderror"
                                           name="amount"
                                           value="{{ $ingredient->amount }}" required autocomplete="amount" autofocus>

                                    @error('amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="amount_type"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Amount type') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control @error('amount_type') is-invalid @enderror"
                                            name="amount_type" required>
                                        <option value="grams"
                                                @if($ingredient->amount_type == 'grams')selected @endif>{{__('Grams')}}</option>
                                        <option value="ml"
                                                @if($ingredient->amount_type == 'ml')selected @endif>{{__('Ml/s')}}</option>
                                        <option value="liter"
                                                @if($ingredient->amount_type == 'liter')selected @endif>{{__('Liter/s')}}</option>
                                        <option value="oz"
                                                @if($ingredient->amount_type == 'oz')selected @endif>{{__('Ounce/s')}}</option>
                                        <option value="piece"
                                                @if($ingredient->amount_type == 'piece')selected @endif>{{__('Piece/s')}}</option>
                                    </select>
                                    @error('amount_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create New') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
