@extends('adminlte::page')
@section('Title', __('Muscle Groups'))

@section('content')
    <div class="container">
        <h1 class="text-center mt-3">{{__('Ingredients')}}</h1>
        <div class="mb-3 mt-3 text-right">
            <a href="{{route('product_ingredients.create')}}"
               class="btn btn-primary float-end">{{__('Create new')}}
            </a>
        </div>
        <div class="row justify-content-center">
            <table class="table table-dark">
                <thead>
                <th>{{__('ID')}}</th>
                <th>{{__('Product')}}</th>
                <th>{{__('Amount')}}</th>
                <th>{{__('Amount_type')}}</th>
                <th>{{__('Actions')}}</th>
                </thead>
                <tbody>
                @foreach( $ingredients as  $ingredient)
                    <tr>
                        <td>{{$ingredient->id}}</td>
                        <td>{{( $ingredient->foodProduct->name)}}</td>
                        <td>{{( $ingredient->amount)}}</td>
                        <td>{{( $ingredient->amount_type)}}</td>
                        <td>
                            <div class="d-flex">
                                <div class="edit mx-2">
                                    <a href="{{route('product_ingredients.edit',  $ingredient->id)}}" class="btn btn-primary">{{__('Edit')}}</a>
                                </div>
                                <form  method="post" action="{{route('product_ingredients.delete',  $ingredient->id)}}" class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger"
                                            onclick="return confirm('Are you sure you want to delete this item?');">{{__('DELETE')}}</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
