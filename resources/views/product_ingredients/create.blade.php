@extends('adminlte::page')
@section('title', __('Create New Ingredient').' - '.env('APP_NAME'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 pt-3">
                <div class="card">
                    <h5 class="card-header">{{__('Create New Ingredient')}}</h5>
                    <div class="card-body">
                        <form method="post" action="{{route('product_ingredients.store')}}"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="row mb-3">
                                <label for="food_product_id"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control @error('food_product_id') is-invalid @enderror"
                                            name="food_product_id" required>
                                        <option value="" selected disabled>{{__('Choose food product')}}...</option>
                                        @foreach(\App\Models\FoodProduct::all() as $product)
                                            <option value="{{$product->id}}">{{$product->name}}</option>
                                        @endforeach
                                    </select>

                                    @error('food_product_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="amount"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Amount') }}</label>
                                <div class="col-md-6">
                                    <input id="amount" type="number"
                                           class="form-control @error('amount') is-invalid @enderror"
                                           name="amount"
                                           value="{{ old('amount') }}" required autocomplete="amount" autofocus>

                                    @error('amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="amount_type"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Amount type') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control @error('amount_type') is-invalid @enderror"
                                            name="amount_type" required>
                                        <option value="" selected disabled>{{__('Choose amount type')}}...</option>
                                        <option value="grams">{{__('Grams')}}</option>
                                        <option value="ml">{{__('Ml/s')}}</option>
                                        <option value="liter">{{__('Liter/s')}}</option>
                                        <option value="oz">{{__('Ounce/s')}}</option>
                                        <option value="piece">{{__('Piece/s')}}</option>
                                    </select>
                                    @error('amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create New') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
