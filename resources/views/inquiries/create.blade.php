@extends('adminlte::page')

@section('title', __('Create New Inquiry').' - '.env('APP_NAME'))

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8 pt-3">
            <div class="card">
                <h5 class="card-header">{{__('Create Inquiry')}}</h5>
                <div class="card-body">
                    <form method="POST" action="{{route('inquiries.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                        </div>
                        <div class="row mb-3">
                            <label for="title" class="col-md-4 col-form-label text-md-end">
                                {{__('Title')}}
                            </label>
                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror"
                                       name="title" value="{{ old('title') }}" required
                                       autocomplete="name" autofocus>
                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="sender_name" class="col-md-4 col-form-label text-md-end">
                                {{__('Customer name')}}
                            </label>
                            <div class="col-md-6">
                                <input id="sender_name" type="text"
                                       class="form-control @error('sender_name') is-invalid @enderror"
                                       name="sender_name" value="{{ old('sender_name') }}" required
                                       autocomplete="name" autofocus>
                                @error('sender_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="sender_phone_number" class="col-md-4 col-form-label text-md-end">
                                {{__('Phone number')}}
                            </label>
                            <div class="col-md-6">
                                <input id="sender_phone_number" type="text"
                                       class="form-control @error('title') is-invalid @enderror"
                                       name="sender_phone_number" value="{{ old('sender_phone_number') }}" required
                                       autocomplete="name" autofocus>
                                @error('sender_phone_number')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="sender_email" class="col-md-4 col-form-label text-md-end">
                                {{__('Email')}}
                            </label>
                            <div class="col-md-6">
                                <input id="sender_email" type="sender_email" class="form-control @error('sender_email') is-invalid @enderror"
                                       name="sender_email" value="{{ old('sender_email') }}" required
                                       autocomplete="name" autofocus>
                                @error('sender_email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="content" class="col-md-4 col-form-label text-md-end">
                                {{__('Content')}}
                            </label>
                            <div class="col-md-6">
                                <textarea
                                    class="form-control @error('content') is-invalid @enderror"
                                    name="content" required
                                    autocomplete="name" autofocus></textarea>

                                @error('content')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        {{--status is hidden-> always status:pending on submit--}}
                        <input type="hidden"
                               id="status"
                               name="status"
                               value="pending">
                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create Inquiry') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection
