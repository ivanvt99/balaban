@extends('adminlte::page')

@section('title', $inquiry->title)

@section('content')

<div class="container col-10 mt-3">
    <div class="row justify-content-center">
        <div class="card" style="background: #fff4d3; padding: 0">
            <div class="d-flex justify-content-between">
                <section class="dealer text-left mt-2 pl-3 ">
                    <p class="dealers text-left mb-1"> {{__('About Product: ')}}
                        <b>{{is_null($inquiry->product_id) ? 'No product' : $inquiry->product->name }}</b></p>
                    <p class="dealers text-left mb-1"> {{__('Addressed to: ')}}
                        <b>Olympus Gym</b></p>
                    <p class="dealers text-left mb-1"> {{__('Sent at: ')}}
                        <b>{{$inquiry->created_at}}</b></p>
                    <hr>
                </section>
                <section class="client-section text-right mt-2 pr-3">
                    <p class="clients text-left mb-1"> {{__('From: ')}}
                        <b>{{$inquiry->sender_name}}</b></p>
                    <p class="clients text-left mb-1"> {{__('Email: ')}}
                        <b>{{$inquiry->sender_email}}</b></p>
                    <p class="clients text-left mb-1"> {{__('Phone number: ')}}
                        <b>{{$inquiry->sender_phone_number}}</b></p>
                    <hr>
                    <p class="clients text-left mb-0"> {{__('Inquiry status: ')}}
                        <b>{{$inquiry->status}}</b></p>
                </section>
            </div>

            <h5 class="text-center"><b>{{$inquiry->title}}</b></h5>
            <div class="card-body">
                {{$inquiry->content}}
                <p class="regards text-right"> {{__('With regards:')}}<br>
                    <b>{{$inquiry->sender_name}}</b></p>
            </div>

            <div class="card-footer" >
                <div class="d-flex justify-content-center">
                    <div class="text-center">
                        @can('inquiry-edit')
                            <a href="{{route('inquiries.edit', $inquiry->id)}})}}"
                               class="btn btn-primary">{{__('Edit Status')}}</a>
                        @endcan
                    </div>

                    <div class="text-center">
                        <a href="{{route('inquiries.index')}}" class="btn btn-info mx-2">{{__('Back to all')}}</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
