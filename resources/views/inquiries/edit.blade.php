@extends('adminlte::page')

@section('title', __('Edit Inquiry').' - '.env('APP_NAME'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 pt-3">
                <div class="card p-3">
                    <h5 class="card-header">{{__('Edit inquiry')}}</h5>
                    <div class="card-bdy">
                        <form method="POST" action="{{route('inquiries.update', $inquiry->id)}}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row mb-3 mt-3">
                                <label for="title" class="col-md-4 col-form-label text-md-end">
                                    {{__('Title')}}
                                </label>
                                <div class="col-md-6">
                                    <input id="title"
                                           type="text"
                                           class="form-control"
                                           name="title"
                                           value="{{$inquiry->title}}"
                                          readonly required>
                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="sender_name" class="col-md-4 col-form-label text-md-end">
                                    {{__('Sender name')}}
                                </label>
                                <div class="col-md-6">
                                    <input id="sender_name" type="text" class="form-control" name="sender_name"
                                           value="{{$inquiry->sender_name}}" readonly required>

                                    @error('sender_name')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="sender_phone_number" class="col-md-4 col-form-label text-md-end">
                                    {{__('Phone number')}}
                                </label>
                                <div class="col-md-6">
                                    <input id="sender_phone_number" type="text" class="form-control" name="sender_phone_number"
                                           value="{{$inquiry->sender_phone_number}}"  readonly required>
                                    @error('sender_phone_number')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="email" class="col-md-4 col-form-label text-md-end">
                                    {{__('Email')}}
                                </label>

                                <div class="col-md-6">
                                    <input id="sender_email" type="sender_email" class="form-control" name="sender_email"
                                           value="{{$inquiry->sender_email}}" readonly required>
                                    @error('sender_email')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="content" class="col-md-4 col-form-label text-md-end">
                                    {{__('Content')}}
                                </label>
                                <div class="col-md-6">
                                    <textarea
                                        class="form-control @error('content') is-invalid @enderror"
                                        name="content" required
                                        autocomplete="name" autofocus readonly>{{$inquiry->content}}</textarea>

                                    @error('content')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-5">
                                <label for="status"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Status') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control @error('status') is-invalid @enderror" name="status" required>
                                        <option value="pending" @if($inquiry->status == 'pending') selected @endif>{{__('Pending')}}</option>
                                        <option value="in_process" @if($inquiry->status == 'in_process') selected @endif>{{__('In Process')}}</option>
                                        <option value="answered" @if($inquiry->status == 'answered') selected @endif>{{__('Answered')}}</option>
                                    </select>

                                    @error('status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3 justify-content-center">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
