@extends('adminlte::page')
@section('title', __('Inquiries'))
@section('content')

    <div class="row justify-content-center">
        <h1 class="text-center">{{__('Inquiries')}}</h1>
        <div class="col-12">
            @can('inquiry-create')
                <a href="{{route('inquiries.create')}}"
                   class="btn btn-primary mt-3 mb-3 float-right">{{__('Create New')}}</a>
            @endcan

            <table class="table table-hover">
                <thead>
                <th></th>
                <th>{{__('From')}}</th>
                <th>{{__('Subject')}}</th>
                <th>{{__('Content')}}</th>
                <th>{{__('Status')}}</th>
                <th class="text-right">{{__('Date')}}</th>
                <th class="text-center">{{__('Actions')}}</th>
                </thead>
                <tbody>
                @foreach($inquiries as $inquiry)
                    <tr class="clickable-row @if(!$inquiry->was_seen) text-bold @endif" style="cursor: pointer"
                        data-href="{{route('inquiries.show',$inquiry->id)}}">
                        <td class="inbox-small-cells">
                            <input type="checkbox" class="mail-checkbox">
                        </td>
                        <td>
                            @if($inquiry->sender_name)
                                {{$inquiry->sender_name}}
                            @else
                                {{__('No Data')}}
                            @endif
                        </td>
                        <td href="{{route('inquiries.show',$inquiry->id)}}">
                            @if($inquiry->title)
                                {{$inquiry->title}}
                            @else
                                {{__('No Data')}}
                            @endif
                        </td>
                        <td>
                            @if($inquiry->content)
                                {{  Str::limit($inquiry->content, 20)}}
                            @else
                                {{__('No Data')}}
                            @endif
                        </td>
                        <td>
                            @if($inquiry->status)
                                {{$inquiry->status}}
                            @else
                                {{__('No Data')}}
                            @endif
                        </td>
                        <td class="text-right">{{$inquiry->created_at->format('d/m/Y - H:s')}}</td>
                        <td>
                            <div class="text-center">
                                @can('inquiry-edit')
                                    <a href="{{route('inquiries.edit', $inquiry->id)}})}}"
                                       class="btn btn-primary">{{__('Edit Status')}}</a>
                                @endcan
                            </div>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
{{--    {{ $inquiries->links('pagination::bootstrap-5') }}--}}
@endsection

@section('js')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".clickable-row").click(function () {
                window.location = $(this).data("href");
            });
        });
    </script>
@endsection
