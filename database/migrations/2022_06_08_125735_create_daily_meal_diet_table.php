<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_meal_diet', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('daily_meal_id');
            $table->unsignedBigInteger('diet_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('daily_meal_id')->references('id')->on('daily_meals');
            $table->foreign('diet_id')->references('id')->on('diets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_meal_diet');
    }
};
