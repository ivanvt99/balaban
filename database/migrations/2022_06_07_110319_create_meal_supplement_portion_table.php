<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_supplement_portion', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('meal_id');
            $table->unsignedBigInteger('supplement_portion_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('meal_id')->references('id')->on('meals');
            $table->foreign('supplement_portion_id')->references('id')->on('supplement_portions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_supplement_portion');
    }
};
