<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_ingredient_recipe', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_ingredient_id');
            $table->unsignedBigInteger('recipe_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('product_ingredient_id')->references('id')->on('product_ingredients');
            $table->foreign('recipe_id')->references('id')->on('recipes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_ingredient_recipe');
    }
};
