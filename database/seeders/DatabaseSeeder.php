<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            FoodCategorySeeder::class,
            ProductSeeder::class,
            ProductIngredientSeeder::class,
            MuscleGroupSeeder::class,
            RoleSeeder::class,
            PermissionSeeder::class,
            UserSeeder::class,
            TrainerSeeder::class,
            ClientSeeder::class,
            SupplemlentSeeder::class,
            SupplementPortionSeeder::class,
        ]);
    }
}
