<?php

namespace Database\Seeders;

use App\Models\MuscleGroup;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Services\SlugService;

class MuscleGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      MuscleGroup::create([
          'name' => 'Chest',
          'slug' => SlugService::createSlug(MuscleGroup::class, 'slug', 'Chest'),
      ]);

        MuscleGroup::create([
            'name' => 'Back',
            'slug' => SlugService::createSlug(MuscleGroup::class, 'slug', 'Back'),
        ]);

        MuscleGroup::create([
            'name' => 'Biceps',
            'slug' => SlugService::createSlug(MuscleGroup::class, 'slug', 'Biceps'),
        ]);

        MuscleGroup::create([
            'name' => 'Triceps',
            'slug' => SlugService::createSlug(MuscleGroup::class, 'slug', 'Triceps'),
        ]);

        MuscleGroup::create([
            'name' => 'Abs',
            'slug' => SlugService::createSlug(MuscleGroup::class, 'slug', 'Abs'),
        ]);

        MuscleGroup::create([
            'name' => 'Ties',
            'slug' => SlugService::createSlug(MuscleGroup::class, 'slug', 'Ties'),
        ]);
    }
}
