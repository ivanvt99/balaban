<?php

namespace Database\Seeders;

use App\Models\ProductIngredient;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductIngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductIngredient::create([
            'food_product_id' => 1,
            'amount'          => 30,
            'amount_type'     => 'grams'
        ]);

        ProductIngredient::create([
            'food_product_id' => 2,
            'amount'          => 5,
            'amount_type'     => 'piece'
        ]);

        ProductIngredient::create([
            'food_product_id' => 6,
            'amount'          => 100,
            'amount_type'     => 'ml'
        ]);
    }
}
