<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    protected $toTruncate = ['permissions'];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::findByName('admin');

        $admin->givePermissionTo(Permission::create(['name' => 'user-view']));
        $admin->givePermissionTo(Permission::create(['name' => 'user-create']));
        $admin->givePermissionTo(Permission::create(['name' => 'user-edit']));
        $admin->givePermissionTo(Permission::create(['name' => 'user-delete']));

        $admin->givePermissionTo(Permission::create(['name' => 'permission-view']));
        $admin->givePermissionTo(Permission::create(['name' => 'permission-create']));
        $admin->givePermissionTo(Permission::create(['name' => 'permission-edit']));
        $admin->givePermissionTo(Permission::create(['name' => 'permission-delete']));

        $admin->givePermissionTo(Permission::create(['name' => 'role-view']));
        $admin->givePermissionTo(Permission::create(['name' => 'role-create']));
        $admin->givePermissionTo(Permission::create(['name' => 'role-edit']));
        $admin->givePermissionTo(Permission::create(['name' => 'role-delete']));

        $admin->givePermissionTo(Permission::create(['name' => 'trainer-view']));
        $admin->givePermissionTo(Permission::create(['name' => 'trainer-create']));
        $admin->givePermissionTo(Permission::create(['name' => 'trainer-edit']));
        $admin->givePermissionTo(Permission::create(['name' => 'trainer-delete']));
        $admin->givePermissionTo(Permission::create(['name' => 'trainer-set-status']));

        $admin->givePermissionTo(Permission::create(['name' => 'client-view']));
        $admin->givePermissionTo(Permission::create(['name' => 'client-create']));
        $admin->givePermissionTo(Permission::create(['name' => 'client-edit']));
        $admin->givePermissionTo(Permission::create(['name' => 'client-delete']));
        $admin->givePermissionTo(Permission::create(['name' => 'client-set-status']));

        $admin->givePermissionTo(Permission::create(['name' => 'recipe-view']));
        $admin->givePermissionTo(Permission::create(['name' => 'recipe-create']));
        $admin->givePermissionTo(Permission::create(['name' => 'recipe-edit']));
        $admin->givePermissionTo(Permission::create(['name' => 'recipe-delete']));

        $admin->givePermissionTo(Permission::create(['name' => 'supplement-view']));
        $admin->givePermissionTo(Permission::create(['name' => 'supplement-create']));
        $admin->givePermissionTo(Permission::create(['name' => 'supplement-edit']));
        $admin->givePermissionTo(Permission::create(['name' => 'supplement-delete']));

        $admin->givePermissionTo(Permission::create(['name' => 'supplement-portion-view']));
        $admin->givePermissionTo(Permission::create(['name' => 'supplement-portion-create']));
        $admin->givePermissionTo(Permission::create(['name' => 'supplement-portion-edit']));
        $admin->givePermissionTo(Permission::create(['name' => 'supplement-portion-delete']));

        $admin->givePermissionTo(Permission::create(['name' => 'meal-view']));
        $admin->givePermissionTo(Permission::create(['name' => 'meal-create']));
        $admin->givePermissionTo(Permission::create(['name' => 'meal-edit']));
        $admin->givePermissionTo(Permission::create(['name' => 'meal-delete']));

        $admin->givePermissionTo(Permission::create(['name' => 'daily-meal-view']));
        $admin->givePermissionTo(Permission::create(['name' => 'daily-meal-create']));
        $admin->givePermissionTo(Permission::create(['name' => 'daily-meal-edit']));
        $admin->givePermissionTo(Permission::create(['name' => 'daily-meal-delete']));

        $admin->givePermissionTo(Permission::create(['name' => 'diet-view']));
        $admin->givePermissionTo(Permission::create(['name' => 'diet-create']));
        $admin->givePermissionTo(Permission::create(['name' => 'diet-show']));
        $admin->givePermissionTo(Permission::create(['name' => 'diet-edit']));
        $admin->givePermissionTo(Permission::create(['name' => 'diet-delete']));


        $admin->givePermissionTo(Permission::create(['name' => 'food-category-view']));
        $admin->givePermissionTo(Permission::create(['name' => 'food-category-create']));
        $admin->givePermissionTo(Permission::create(['name' => 'food-category-edit']));
        $admin->givePermissionTo(Permission::create(['name' => 'food-category-delete']));

        $admin->givePermissionTo(Permission::create(['name' => 'food-view']));
        $admin->givePermissionTo(Permission::create(['name' => 'food-create']));
        $admin->givePermissionTo(Permission::create(['name' => 'food-edit']));
        $admin->givePermissionTo(Permission::create(['name' => 'food-delete']));

        $admin->givePermissionTo(Permission::create(['name' => 'ingredient-view']));
        $admin->givePermissionTo(Permission::create(['name' => 'ingredient-create']));
        $admin->givePermissionTo(Permission::create(['name' => 'ingredient-edit']));
        $admin->givePermissionTo(Permission::create(['name' => 'ingredient-delete']));

        $admin->givePermissionTo(Permission::create(['name' => 'inquiry-view']));
        $admin->givePermissionTo(Permission::create(['name' => 'inquiry-create']));
        $admin->givePermissionTo(Permission::create(['name' => 'inquiry-edit']));
        $admin->givePermissionTo(Permission::create(['name' => 'inquiry-delete']));

        $trainer = Role::findByName('trainer');
        $trainer->givePermissionTo('client-view');
        $trainer->givePermissionTo('client-create');
        $trainer->givePermissionTo('client-edit');
        $trainer->givePermissionTo('client-delete');
        $trainer->givePermissionTo('client-set-status');

        $trainer->givePermissionTo('recipe-view');
        $trainer->givePermissionTo('recipe-create');
        $trainer->givePermissionTo('recipe-edit');
        $trainer->givePermissionTo('recipe-delete');

        $trainer->givePermissionTo('supplement-view');
        $trainer->givePermissionTo('supplement-create');
        $trainer->givePermissionTo('supplement-edit');
        $trainer->givePermissionTo('supplement-delete');

        $trainer->givePermissionTo('supplement-portion-view');
        $trainer->givePermissionTo('supplement-portion-create');
        $trainer->givePermissionTo('supplement-portion-edit');
        $trainer->givePermissionTo('supplement-portion-delete');

        $trainer->givePermissionTo('meal-view');
        $trainer->givePermissionTo('meal-create');
        $trainer->givePermissionTo('meal-edit');
        $trainer->givePermissionTo('meal-delete');

        $trainer->givePermissionTo('daily-meal-view');
        $trainer->givePermissionTo('daily-meal-create');
        $trainer->givePermissionTo('daily-meal-edit');
        $trainer->givePermissionTo('daily-meal-delete');

        $trainer->givePermissionTo('diet-view');
        $trainer->givePermissionTo('diet-create');
        $trainer->givePermissionTo('diet-show');
        $trainer->givePermissionTo('diet-edit');
        $trainer->givePermissionTo('diet-delete');


        $trainer->givePermissionTo( 'food-category-view');
        $trainer->givePermissionTo( 'food-category-create');
        $trainer->givePermissionTo( 'food-category-edit');
        $trainer->givePermissionTo( 'food-category-delete');

        $trainer->givePermissionTo( 'food-view');
        $trainer->givePermissionTo( 'food-create');
        $trainer->givePermissionTo( 'food-edit');
        $trainer->givePermissionTo( 'food-delete');

        $trainer->givePermissionTo( 'ingredient-view');
        $trainer->givePermissionTo( 'ingredient-create');
        $trainer->givePermissionTo( 'ingredient-edit');
        $trainer->givePermissionTo( 'ingredient-delete');

        $trainer->givePermissionTo('inquiry-view');
        $trainer->givePermissionTo('inquiry-edit');
        $trainer->givePermissionTo('inquiry-create');

        $client = Role::findByName('client');
        $client->givePermissionTo('inquiry-create');
    }
}
