<?php

namespace Database\Seeders;

use App\Models\FoodProduct;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FoodProduct::create([
            'name'             => 'Apple',
            'calories'         => 180,
            'carbs'            => 50,
            'protein'          => 3,
            'fat'              => 1,
            'food_category_id' => 1,
        ]);

        FoodProduct::create([
            'name'             => 'Kiwi',
            'calories'         => 180,
            'carbs'            => 50,
            'protein'          => 3,
            'fat'              => 1,
            'food_category_id' => 1,
        ]);

        FoodProduct::create([
            'name'             => 'Banana',
            'calories'         => 180,
            'carbs'            => 150,
            'protein'          => 20,
            'fat'              => 10,
            'food_category_id' => 1,
        ]);

        FoodProduct::create([
            'name'             => 'Pear',
            'calories'         => 195,
            'carbs'            => 80,
            'protein'          => 10,
            'fat'              => 3,
            'food_category_id' => 1,
        ]);

        FoodProduct::create([
            'name'             => 'Egg',
            'calories'         => 80,
            'carbs'            => 30,
            'protein'          => 20,
            'fat'              => 5,
            'food_category_id' => 4,
        ]);

        FoodProduct::create([
            'name'             => 'Milk',
            'calories'         => 400,
            'carbs'            => 80,
            'protein'          => 32,
            'fat'              => 3,
            'food_category_id' => 5,
        ]);

    }
}
