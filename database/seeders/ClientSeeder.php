<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     Client::create ([
        'user_id' => 4,
        'trainer_id' => 2,
        'client_name' => 'Alexander Kapra',
        'address' => 'Kozlodui str., 5000 Veliko Tarnovo, Bulgaria',
        'status' => 'active'
     ]);

        Client::create ([
            'user_id' => 5,
            'trainer_id' => 1,
            'client_name' => 'Angel Stoqnov',
            'address' => 'Dr.Tzonchev str., 5000 Veliko Tarnovo, Bulgaria',
            'status' => 'inactive'
        ]);
    }
}
