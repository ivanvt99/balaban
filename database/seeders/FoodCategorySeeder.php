<?php

namespace Database\Seeders;

use App\Models\FoodCategory;
use App\Services\SlugService;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FoodCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FoodCategory::create([
            'name' => 'Fruits',
            'slug' => SlugService::createSlug(FoodCategory::class, 'slug', 'Fruits'),
        ]);

        FoodCategory::create([
            'name' => 'Vegetables',
            'slug' => SlugService::createSlug(FoodCategory::class, 'slug', 'Vegetables'),
        ]);

        FoodCategory::create([
            'name' => 'Meat Products',
            'slug' => SlugService::createSlug(FoodCategory::class, 'slug', 'Meat Products'),
        ]);

        FoodCategory::create([
            'name' => 'Eggs',
            'slug' => SlugService::createSlug(FoodCategory::class, 'slug', 'Eggs'),
        ]);

        FoodCategory::create([
            'name' => 'Dairy Products',
            'slug' => SlugService::createSlug(FoodCategory::class, 'slug', 'Dairy Products'),
        ]);
    }
}
