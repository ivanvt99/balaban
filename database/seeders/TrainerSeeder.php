<?php

namespace Database\Seeders;

use App\Models\Trainer;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TrainerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Trainer::create([
            'user_id'      => 2,
            'trainer_name'  => 'Emil Popov',
            'address'      => '36 Balaban, Gorna Orjahovitsa, 5005 ',
            'email'        => 'emo@balaban.com',
            'status'       => 'active']);


        Trainer::create([
            'user_id'      => 3,
            'trainer_name'  => 'Georgi Stanchev',
            'address'      => '89 Javorov.str, Veliko Tarnovo, 5000',
            'email'        => 'g.stanchev@balaban.com',
            'status'       => 'active']);
    }
}
