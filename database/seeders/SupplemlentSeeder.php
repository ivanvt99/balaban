<?php

namespace Database\Seeders;

use App\Models\Supplement;
use App\Services\SlugService;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SupplemlentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Supplement::create([
            'name'        => 'Whey Protein',
            'description' => 'Whey protein is a mixture of proteins isolated from whey,
           the liquid material created as a by-product of cheese production.
           The proteins consist of α-lactalbumin, β-lactoglobulin,
           serum albumin and immunoglobulins.',
            'slug'        => SlugService::createSlug(Supplement::class, 'slug', 'Whey Protein')
        ])->addMedia(public_path('images\cropped-hand-holding-ground-protein-in-scoop-at-royalty-free-image-1589373439.jpg'))->preservingOriginal()->toMediaCollection('supplement');

        Supplement::create([
            'name'        => 'Creatine',
            'description' => 'Creatine is an organic compound. It exists in various modifications (tautomers) in solution.
             Creatine is found in vertebrates where it facilitates recycling of adenosine triphosphate (ATP),
             primarily in muscle and brain tissue. Recycling is achieved by converting adenosine diphosphate
             (ADP) back to ATP via donation of phosphate groups. Creatine also acts as a buffer.',
            'slug'        => SlugService::createSlug(Supplement::class, 'slug', 'Creatine')
        ])->addMedia(public_path('images\iq4ju5ow-900.jpg'))->preservingOriginal()->toMediaCollection('supplement');

        Supplement::create([
            'name'        => 'Glutamine',
            'description' => 'Glutamine is an α-amino acid that is used in the biosynthesis of proteins.
             Its side chain is similar to that of glutamic acid, except the carboxylic acid group is replaced by an amide.
             It is classified as a charge-neutral, polar amino acid. It is non-essential and conditionally essential in humans,
             meaning the body can usually synthesize sufficient amounts of it, but in some instances of stress, the bodys
             demand for glutamine increases, and glutamine must be obtained from the diet. It is encoded by the codons CAA and CAG.',
            'slug'        => SlugService::createSlug(Supplement::class, 'slug', 'Glutamine')
        ])->addMedia(public_path('images\glutamine-amino-acid-nutrition-bodybuilding-fitness-supplements-black-stone-background-glutamine-amino-acid-nutrition-136546440.jpg'))->preservingOriginal()->toMediaCollection('supplement');

    }
}
