<?php

namespace Database\Seeders;

use App\Models\SupplementPortion;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SupplementPortionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SupplementPortion::create([
            'supplement_id' => 1,
            'amount'        => 20,
            'amount_type'   => 'grams'
        ]);

        SupplementPortion::create([
            'supplement_id' => 2,
            'amount'        => 5,
            'amount_type'   => 'grams'
        ]);

        SupplementPortion::create([
            'supplement_id' => 3,
            'amount'        => 3,
            'amount_type'   => 'scoop'
        ]);
    }
}
