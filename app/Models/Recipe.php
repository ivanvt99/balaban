<?php

namespace App\Models;

use App\Http\Controllers\MealController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Recipe extends Model implements HasMedia
{
    use InteractsWithMedia;
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'preparation_time',
        'cooking_time',
        'time_amount',
        'calories'
    ];

    public function productIngredients()
    {
        return $this->belongsToMany(ProductIngredient::class);
    }
    public function meals()
    {
        return $this->belongsToMany(MealController::class);
    }
}
