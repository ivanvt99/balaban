<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    use HasFactory;
    protected $fillable =
        [
            'title',
            'sender_name',
            'sender_phone_number',
            'sender_email',
            'content',
            'trainer_id',
            'was_seen',
            'status'
        ];
//    public function trainer()
//    {
//        return $this->belongsTo(Trainer::class);
//    }
}
