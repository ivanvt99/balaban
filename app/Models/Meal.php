<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Meal extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'meal_calories',
        'notes',
    ];

    public function recipes()
    {
        return$this->belongsToMany(Recipe::class);
    }
    public function productIngredients()
    {
        return $this->belongsToMany(ProductIngredient::class);
    }
    public function supplementPortions()
    {
        return $this->belongsToMany(SupplementPortion::class);
    }

    public function dailyMeals()
    {
        return $this->belongsToMany(DailyMeal::class);
    }

}
