<?php

namespace App\Models;

use App\Http\Controllers\MealController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductIngredient extends Model
{
    use HasFactory;
    protected  $fillable = [
        'food_product_id',
        'amount',
        'amount_type',
    ];

    public function foodProduct()
    {
        return $this->belongsTo(FoodProduct::class);
    }

    public function recipes()
    {
     return $this->belongsToMany(Recipe::class);
    }
    public function meals()
    {
        return $this->belongsToMany(MealController::class);
    }
}
