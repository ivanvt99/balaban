<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FoodProduct extends Model
{
    use HasFactory;

     protected $fillable = [
         'name',
         'calories',
         'carbs',
         'protein',
         'fat',
         'food_category_id'
     ];

     public function foodCategory(){
         return $this->belongsTo(FoodCategory::class);
     }
}
