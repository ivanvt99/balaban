<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DailyMeal extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'day_name',
        'daily_calories',
        'target_calories',
    ];
    public function meals()
    {
        return $this->belongsToMany(Meal::class);
    }

    public function diets()
    {
        return $this->belongsToMany(Diet::class);
    }
}
