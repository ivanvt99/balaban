<?php

namespace App\Models;

use App\Http\Controllers\MealController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SupplementPortion extends Model
{
    use HasFactory;
    protected $fillable = ['supplement_id', 'amount', 'amount_type'];

    public function supplement()
    {
        return $this->belongsTo(Supplement::class);
    }

    public function meals()
    {
        return $this->belongsToMany(MealController::class);
    }
}
