<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class DailyMealRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'day_name'        => 'required',
            'meals'           => 'required',
            'daily_calories'  => 'required|numeric|gt:0',
            'target_calories' => 'nullable',
        ];
    }

    public function messages()
    {
        $rules = [
            'day_name.required'        => ' A name of the  day is required',
            'meals.required'           => 'At least one meal is required',
            'daily_calories.required'  => 'calories for the day are required',
            'target_calories.required' => ' Target calories for the day are required',
        ];
        return $rules;
    }

    protected function failedValidation(Validator $validator)
    {
//        dd($this->errorBag,$this->getRedirectUrl(),  $validator);
        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
