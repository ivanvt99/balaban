<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExerciseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'muscle_group_id' => 'required',
            'name' => 'required|min:3',
            'description' => 'required|min:10',
        ];
    }

    public function messages()
    {
        $rules = [
            'muscle_group_id.required' => 'Muscle Group is required',
            'name.required' => 'The  name must be at least 3 characters long',
            'description.required' => 'The  description must be at least 10 characters long'
        ];
        return $rules;
    }
}
