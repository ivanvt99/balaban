<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MealRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'          => 'required',
            'meal_calories' => 'required|numeric|gt:0',
            'recipes'       => 'required',
            'ingredients'   => 'nullable',
            'portions'      => 'nullable',
        ];
    }

    public function messages()
    {
        $rules = [
            'recipe_id.required'     => 'A recipe is required',
            'name.required'          => 'Meal name is required',
            'meal_calories.required' => 'Calories for the meal are required',
        ];
        return $rules;
    }
}
