<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupplementPortionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'supplement_id' => 'required',
            'amount'        => 'required|numeric|gt:0',
            'amount_type'   => 'required'
        ];
    }

    public function messages()
    {
        $rules = [
            'supplement_id.required' => 'please Choose a supplement',
            'amount.required'        => 'Please select a amount',
            'amount_type.required'   => 'Choose the amount type',
        ];
        return $rules;
    }
}
