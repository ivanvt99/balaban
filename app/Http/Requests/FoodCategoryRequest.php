<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FoodCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
//            'slug' => 'required',
        ];
    }

    public function messages()
    {
        $rules = [
            'name.required' => 'Food Category name is required',
//            'slug.required' => 'The slug is required',
        ];
        return $rules;
    }
}
