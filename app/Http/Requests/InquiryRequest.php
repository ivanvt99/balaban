<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InquiryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'trainer_id'          => 'nullable',
            'title'               => 'required',
            'sender_name'         => 'required',
            'sender_phone_number' => 'required',
            'sender_email'        => 'required',
            'content'             => 'required',
//            'was_seen'            => 'required',
//            'status'              => 'required'
        ];
    }

    public function messages()
    {
       $rules = [
           'title.required'               => 'Title is required',
           'sender_name.required'         => 'Sender Name required',
           'sender_phone_number.required' => 'Phone number required',
           'sender_email.required'        => 'Email required',
           'content.required'             => 'Your content is required',
       ];
       return $rules;
    }
}
