<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecipeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'             => 'required',
            'description'      => 'required',
            'preparation_time' => 'required|numeric|gt:0',
            'cooking_time'     => 'required|numeric|gt:0',
            'time_amount'      => 'required',
            'ingredients'      => 'required',
            'calories'         => 'required|numeric|gt:0',
        ];
    }

    public function messages()
    {
        $rules = [
            'name.required'             => 'Recipe Name is required',
            'description.required'      => 'Recipe Description is required',
            'preparation_time.required' => 'Recipe Prep Time is required',
            'cooking_time.required'     => 'Recipe Cooking Time is required',
            'time_amount.required'      => 'Recipe Time amount is required',
            'ingredients.required'      => 'Recipe Ingredient is required',
            'calories.required'         => 'Recipe Calories are required',
        ];
        return $rules;
    }
}
