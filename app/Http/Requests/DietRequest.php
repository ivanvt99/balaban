<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DietRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'            => 'required',
            'daily_meals'     => 'required',
            'client_id'       => 'nullable',
            'trainer_id'      => 'required',
            'description'     => 'required',
            'time_range'      => 'required',
            'time_range_type' => 'required',
        ];
    }

    public function messages()
    {
        $rules = [
            'name.required'            => 'Diet name required',
            'daily_meal.required'      => 'At lest 1 daily meal is required',
            'client_id.nullable'       => 'Enter a Client for the Diet',
            'trainer_id.required'      => 'Enter a Publisher for the Diet',
            'description.required'     => 'Diet Description is required',
            'time_range.required'      => 'Time range required',
            'time_range_type.required' => 'Range type isrequired',
        ];
        return $rules;
    }
}
