<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TrainerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'      => 'required',
            'trainer_name' => 'required',
            'address'      => 'required',
            'email'        => 'required',
            'status'       => 'required',
        ];
    }

    public function messages()
    {
        $rules = [
            'user_id.required'      => 'An existing user is required',
            'trainer_name.required' => 'The name of the trainer is required',
            'address.required'      => 'Trainer address is required',
            'email.required'        => 'Trainer email is required',
            'status.required'       => 'Trainer Status required',
        ];
        return $rules;
    }
}
