<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductIngredientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'food_product_id' => 'required',
            'amount'          => 'required|numeric|gt:0',
            'amount_type'     => 'required',
        ];
    }

    public function messages()
    {
        $rules = [
            'food_product_id.required' => 'food product is required',
            'amount.required'          => 'A amount is required',
            'amount_type.required'     => 'A amount type is required',
        ];
        return $rules;
    }
}
