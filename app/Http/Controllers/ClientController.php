<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientRequest;
use App\Models\Client;
use App\Models\ProductIngredient;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class ClientController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:client-view', ['only' => ['index']]);
        $this->middleware('permission:client-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:client-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:client-delete', ['only' => ['delete']]);
    }

    public function index()
    {
        $clients = Client::all();
        return view('clients.index')->with(['clients' => $clients]);
    }

    public function create()
    {
        return view('clients.create');
    }

    public function store(ClientRequest $request)
    {
        $user = User::create([
            'email'        => $request->get('email'),
            'password'     => '$2a$12$t1P8XEMuA7lx9L1ntNm1xusb4tLK8vjfYfL1EF8MCQXS4iNxj3Lnq',
            'first_name'   => $request->get('first_name'),
            'last_name'    => $request->get('last_name'),
            'phone_number' => $request->get('phone_number'),
        ]);
        $user->assignRole('client');
        $client = Client::create([
            'client_name' => $user->first_name . " " . $user->last_name,
            'trainer_id'  => $request->trainer_id,
            'user_id'     => $user->id,
            'address'     => $request->get('address'),
            'status'      => $request->get('status'),
        ]);
        if ($client->status != 'suspended') {
            $user->assignRole('client');
        } else {
            $user->assignRole('suspended-client');
        }

        Alert::success('Great!', 'Client created successfully!');
        return redirect()->action([ClientController::class, 'index']);
    }

    public function edit($id)
    {
        $client = Client::findOrFail($id);
        return view('clients.edit')->with(['client' => $client]);
    }

    public function update(Request $request, $id)
    {
        $client = Client::findOrFail($id);
        $user = $client->user;
        $rules = array(
            'trainer_id'  => 'required',
            'status'      => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect(route('clients.edit', $client->id))->withErrors($validator)->withInput($request->all());
        }
        $client->trainer_id = $request->trainer_id;
        $client->status = $request->get('status');
        $client->save();

        Alert::success('Great!', 'Client updated successfully!');
        return redirect()->action([ClientController::class, 'index']);
    }

    public function delete($id)
    {
        $client = Client::findOrFail($id);
        $userId = $client->user_id;
        $user = User::findOrFail($userId);
        $user->removeRole('client');

        if ($client) {
            $client->delete();
        }
        return redirect()->action([ClientController::class, 'index']);
    }

}
