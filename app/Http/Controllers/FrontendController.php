<?php

namespace App\Http\Controllers;

use App\Models\DailyMeal;
use App\Models\Diet;
use App\Models\Recipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontendController extends Controller
{
    public function recipeCatalogue()
    {
        $recipes  = Recipe::all();
        return view('frontend.recipes')->with(['recipes' => $recipes]);
    }

    public function showRecipe($id)
    {
        $recipe = Recipe::find($id);
        return view('frontend.show_recipe')->with(['recipe' => $recipe]);
    }

    public function dietsCatalogue()
    {
        $diets  = Diet::all();
//            DB::table('diets')->where('client_id', '=', null);
        return view('frontend.diets')->with(['diets' => $diets]);
    }

    public function showDiet($id)
    {
        $diet = Diet::find($id);
        $dailyMeals = $diet->dailyMeals;
        return view('frontend.show_diet')->with(['diet' => $diet, 'dailyMeals' => $dailyMeals]);
    }

    public function showMeal($id)
    {
        $dailyMeal = DailyMeal::find($id);
         $meals = $dailyMeal->meals;
         return view('frontend.meals')->with(['meals' => $meals]);
    }
}
