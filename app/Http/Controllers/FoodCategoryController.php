<?php

namespace App\Http\Controllers;

use App\Http\Requests\FoodCategoryRequest;
use App\Models\FoodCategory;
use App\Services\SlugService;
use Database\Seeders\ProductSeeder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FoodCategoryController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:food-category-view', ['only' => ['index']]);
        $this->middleware('permission:food-category-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:food-category-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:food-category-delete', ['only' => ['delete']]);
    }
    public function index()
    {
        $foodCategories = FoodCategory::all();
        return view('food_categories.index', compact('foodCategories'));
    }
    public function create()
    {
      return view('food_categories.create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',

        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect(route('food_categories.create'))
                ->withErrors($validator)
                ->withInput($request->all());

        }
        else
        {
            FoodCategory::create([
                'name' => $request->get('name'),
                'slug' => SlugService::createSlug(FoodCategory::class, 'slug', $request->get('name')),

            ]);
            return redirect(route('food_categories.index'));
        }
    }
    public function edit($id)
    {
      $foodCategory =  FoodCategory::find($id);
        return view('food_categories.edit', compact('foodCategory'));
    }

    public function update(FoodCategoryRequest $request, $id)
    {
        $foodCategory = FoodCategory::find($id);
        $data = $request->except(['_method', '_token']);
        if ($foodCategory)
        {
            $foodCategory->update($data);
        }
        return redirect()->action([FoodCategoryController::class, 'index']);
    }

    public function delete($id)
    {
        $foodCategory = FoodCategory::find($id);
        $foodCategory->delete();
        return redirect()->action([FoodCategoryController::class, 'index']);
    }


}
