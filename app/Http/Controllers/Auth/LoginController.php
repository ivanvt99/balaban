<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        authenticated as laravelAuthenticated;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request, $user)
    {
        Alert::success('Hello', Auth::user()->first_name);
        if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('trainer')) {
            if (Auth::user()->hasRole('trainer')) {
                if ($user->trainer->status == 'inactive') {
                    $this->guard()->logout();
                    $request->session()->invalidate();
                    Alert::error('Oops!', 'You are inactive! Please call admin!');
                    return redirect('/login');
                }
            }
            return redirect()->route('dashboard');
        }
        elseif (Auth::user()->hasRole('client'))
        {
                if ($user->client->status == 'inactive') {
                    $this->guard()->logout();
                    $request->session()->invalidate();
                    Alert::error('Oops!', 'You are inactive! Call Your Trainer for more info!');
                    return redirect('/login');
                }
        }
        return redirect()->route('welcome');
    }
}
