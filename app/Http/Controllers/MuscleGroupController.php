<?php

namespace App\Http\Controllers;

use App\Models\MuscleGroup;
use App\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MuscleGroupController extends Controller
{
    public function index()
    {
        $muscles = MuscleGroup::all();
        return view('muscle_groups.index', compact('muscles'));
    }

    public function create()
    {
        return view('muscle_groups.create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect(route('muscle_groups.create'))
                ->withErrors($validator)
                ->withInput($request->all());

        }
        else
        {
            MuscleGroup::create([
                'name' => $request->get('name'),
                'slug' => SlugService::createSlug(MuscleGroup::class, 'slug', $request->get('name')),
            ]);
            return redirect(route('muscle_groups.index'));
        }
    }

    public function edit($id)
    {
        $muscle = MuscleGroup::find($id);
        return view('muscle_groups.edit')->with(['muscle' => $muscle]);
    }

    public function update(Request $request, $id)
    {
        $muscle = MuscleGroup::find($id);
        $rules = array(
            'name' => 'required',

        );
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails())
        {
            dd($validator);
            return redirect(route('muscle_groups.edit',$id))
                ->withErrors($validator)
                ->withInput($request->all());
        }
        else
        {
            $muscle->name = $request->get('name');
            $muscle->slug = SlugService::createSlug(MuscleGroup::class,'slug', $request->get('name'));
            $muscle->save();
            return redirect()->action([MuscleGroupController::class, 'index'])->with('success');
        }
    }

    public function delete($id)
    {
        $muscle = MuscleGroup::find($id);
        $muscle->delete();
        return redirect()->action([MuscleGroupController::class, 'index']);
    }
}
