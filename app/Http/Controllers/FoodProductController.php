<?php

namespace App\Http\Controllers;

use App\Models\FoodProduct;
use \Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class FoodProductController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:food-view', ['only' => ['index']]);
        $this->middleware('permission:food-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:food-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:food-delete', ['only' => ['delete']]);
    }

    public function index()
    {
        $foodProducts = FoodProduct::all();
        return view('food_products.index')->with(['foodProducts' => $foodProducts]);
    }

    public function create()
    {
        return view('food_products.create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'             => 'required',
            'calories'         => 'required|numeric|gt:0',
            'carbs'            => 'required|numeric|gt:0',
            'protein'          => 'required|numeric|gt:0',
            'fat'              => 'required|numeric|gt:0',
            'food_category_id' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect(route('food_products.create'))
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            FoodProduct::create([
                'food_category_id' => $request->get('food_category_id'),
                'name'             => $request->get('name'),
                'calories'         => $request->get('calories'),
                'carbs'            => $request->get('carbs'),
                'protein'          => $request->get('protein'),
                'fat'              => $request->get('fat'),
            ]);
            Alert::success('Great', 'Food Product created successfully!');
            return redirect()->action([FoodProductController::class, 'index']);
        }
    }

    public function edit($id)
    {
        $foodProduct = FoodProduct::find($id);
        return view('food_products.edit', compact('foodProduct'));
    }

    public function update(Request $request, $id)
    {
        $foodProduct = FoodProduct::find($id);
        $rules = array(
            'name'             => 'required',
            'calories'         => 'required|numeric|gt:0',
            'carbs'            => 'required|numeric|gt:0',
            'protein'          => 'required|numeric|gt:0',
            'fat'              => 'required|numeric|gt:0',
//            'food_category_id' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect(route('food_products.edit'))
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
//            $foodProduct->food_category = $request->get('food_category');
            $foodProduct = FoodProduct::findOrfail($id);
            $foodProduct->name = $request->get('name');
            $foodProduct->calories = $request->get('calories');
            $foodProduct->carbs = $request->get('carbs');
            $foodProduct->protein = $request->get('protein');
            $foodProduct->fat = $request->get('fat');
            $foodProduct->update();

            if ($foodProduct->calories < 0 || $foodProduct->carbs < 0 || $foodProduct->protein < 0 ||$foodProduct->fat < 0)
            {
                dd('test');
                Alert::error('Wait a minute', 'You are entering wrong data! Please Try again');
                return redirect()->back();
            }
            Alert::success('Great', 'Food Product updated successfully!');
            return redirect()->action([FoodProductController::class, 'index'])->with('food_product', $foodProduct);
        }
    }

    public function delete($id)
    {
        $foodProduct = FoodProduct::find($id);
        $foodProduct->delete();
        Alert::success('Great', 'Food Product deleted successfully!');
        return redirect()->action([FoodProductController::class, 'index']);
    }

}
