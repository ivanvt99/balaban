<?php

namespace App\Http\Controllers;

use App\Http\Requests\RecipeRequest;
use App\Models\Recipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class RecipeController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:recipe-view', ['only' => ['index']]);
        $this->middleware('permission:recipe-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:recipe-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:recipe-delete', ['only' => ['delete']]);
    }

    public function index()
    {
        $recipes = Recipe::all();
        return view('recipes.index')->with(['recipes' => $recipes]);
    }

    public function create()
    {
        return view('recipes.create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'             => 'required',
            'description'      => 'required',
            'preparation_time' => 'required|numeric|gt:0',
            'cooking_time'     => 'required|numeric|gt:0',
            'time_amount'      => 'required',
            'ingredients'      => 'required',
            'image'            => 'required',
            'calories'         => 'required|numeric|gt:0',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect(route('recipes.create'))->withErrors($validator)->withInput($request->all());
        } else {
            $recipe = Recipe::create($request->input());
            $recipe->update();
            $recipe->productIngredients()->sync($request->get('ingredients'));

            $recipe->addMediaFromRequest('image')->toMediaCollection('recipe/' . $recipe->id);
            $recipe->save();
            if ($request->has($request->get('ingredients'))) {
                $recipe->sizes()->sync([$request->get('ingredients')]);

            }

            Alert::success('Great!', 'Recipe created successfully!');
            return redirect()->action([RecipeController::class, 'index']);
        }
    }

    public function edit($id)
    {
        $recipe = Recipe::find($id);
        if (!$recipe)
        {
            Alert::error('Oops!', 'Recipe not found!');
            return redirect()->action([RecipeController::class, 'index']);
        }
        return view('recipes.edit')->with(['recipe' => $recipe]);
    }

    public function update(RecipeRequest $request, $id)
    {
            $recipe = Recipe::find($id);
            $data = $request->except(['_method', '_token']);
            if ($recipe)
            {
                $recipe->productIngredients()->sync($request->get('ingredients'));
                $recipe->update($data);
            }
            if ($request->has('image'))
            {
                $recipe->getMedia('recipe/'.$recipe->id)->first()->delete();
                $recipe->addMediaFromRequest('image')->toMediaCollection('recipe/'.$recipe->id);
            }

        Alert::success('Success!', 'Recipe updated successfully!');
        return redirect()->action([RecipeController::class, 'index']);
    }
    public function delete($id)
    {
        $recipe = Recipe::find($id);
        if ($recipe)
        {
            //TODO fix this check!!!
//            if ($recipe->meals()->exists())
//            {
//                Alert::error('Oops!', 'Recipe couldn\'t be deleted due to related record/s!');
//                return redirect()->back();
//            }
            $recipe->delete();
        }
        Alert::success('Great!', 'Recipe deleted successfully!');
        return redirect()->action([RecipeController::class, 'index']);
    }
}
