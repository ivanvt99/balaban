<?php

namespace App\Http\Controllers;

use App\Http\Requests\DietRequest;
use App\Models\Diet;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class DietController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:diet-view', ['only' => ['index']]);
        $this->middleware('permission:diet-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:diet-show', ['only' => ['show']]);
        $this->middleware('permission:diet-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:diet-delete', ['only' => ['delete']]);
    }

    public function index()
    {
        $diets = Diet::all();
        return view('diets.index')->with(['diets' => $diets]);
    }

    public function create()
    {
        return view('diets.create');
    }

    public function show($id)
    {
        $diets = Diet::find($id);
        $dailyMeals = $diets->dailyMeals;
        return view('daily_meals.index')->with(['dailyMeals' => $dailyMeals]);
    }

    public function store(DietRequest $request)
    {
        $diet = Diet::create($request->input());
        $diet->update();
        $diet->dailyMeals()->sync($request->get('daily_meals'));
        if ($request->has($request->get('daily_meals'))) {
            $diet->dailyMeals()->sync([$request->get('daily_meals')]);
        }
        $diet->addMediaFromRequest('image')->toMediaCollection('diet');
        Alert::success('Great!', 'Diet created successfully!');
        return redirect(route('diets.index'));
    }

    public function edit($id)
    {
        $diet = Diet::find($id);
        return view('diets.edit')->with(['diet' => $diet]);
    }

    public function update(DietRequest $request, $id)
    {
        $data = $request->except('_method', '_token');
        $diet = Diet::find($id);
        if ($diet) {
            $diet->dailyMeals()->sync($request->get('daily_meals'));
            $diet->update($data);
        }
        if ($request->has('image')) {
            $diet->getMedia('diet')->first()->delete();
            $diet->addMediaFromRequest('image')->toMediaCollection('diet');
        }
        Alert::success('Great!', 'Diet updated successfully!');
        return redirect()->action([DietController::class, 'index']);
    }

    public function delete($id)
    {
        $diet = Diet::find($id);
        if (!$diet) {
            Alert::error('Oops!', 'Diet not not found!');
            return redirect()->back();
        }
        $diet->delete();
        Alert::success('Great!', 'Diet deleted successfully!');
        return redirect()->action([DietController::class, 'index']);
    }
}
