<?php

namespace App\Http\Controllers;

use App\Http\Requests\SupplementRequest;
use App\Models\Supplement;
use App\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class SupplementController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:supplement-view', ['only' => ['index']]);
        $this->middleware('permission:supplement-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:supplement-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:supplement-delete', ['only' => ['delete']]);
    }

    public function index()
    {
        $supplements = Supplement::all();
        return view('supplements.index')->with(['supplements' => $supplements]);
    }

    public function create()
    {
        return view('supplements.create');
    }

    public function store(Request $request)
    {

        $rules = array(
            'name'             => 'required',
            'description'      => 'required',
            'image'            => 'required',

        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect(route('supplements.create'))->withErrors($validator)->withInput($request->all());
        } else {
           $supplement = Supplement::create([
               'name' => $request->get('name'),
               'description' => $request->get('description'),
               'slug' => SlugService::createSlug(Supplement::class, 'slug', $request->get('name')),
           ]);
//       $supplement->addMediaFromRequest('image')->toMediaCollection('supplement/' . $supplement->id);
       $supplement->addMediaFromRequest('image')->toMediaCollection('supplement');

        Alert::success('Great!', 'Supplement created successfully!');
        return redirect(route('supplements.index'));
        }
    }

    public function edit($id)
    {
        $supplement = Supplement::find($id);
        return view('supplements.edit')->with(['supplement' => $supplement]);
    }

    public function update(SupplementRequest $request, $id)
    {
        $data = $request->except('_method', '_token');
        $supplement = Supplement::find($id);
        if ($supplement)
        {
            $supplement->update($data);
        }
        if ($request->has('image'))
        {
            $supplement->getMedia('supplement')->first()->delete();
            $supplement->addMediaFromRequest('image')->toMediaCollection('supplement');
        }
        Alert::success('Great!', 'Supplement updated successfully!');
        return redirect()->action([SupplementController::class, 'index']);
    }

    public function delete($id)
    {
        $supplement = Supplement::find($id);
        if (!$supplement)
        {
            Alert::error('Oops!', 'Supplement not found!');
        }
        $supplement->delete();
        Alert::success('Great!', 'Supplement deleted successfully!');
        return redirect()->back();
    }
}
