<?php

namespace App\Http\Controllers;

use App\Http\Requests\DailyMealRequest;
use App\Models\DailyMeal;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class DailyMealController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:daily-meal-view', ['only' => ['index']]);
        $this->middleware('permission:daily-meal-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:daily-meal-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:daily-meal-delete', ['only' => ['delete']]);
    }

    public function index()
    {
        $dailyMeals = DailyMeal::all();
        return view('daily_meals.index')->with(['dailyMeals' => $dailyMeals]);
    }

    public function show($id)
    {
        $dailyMeals = DailyMeal::find($id);
        $meals = $dailyMeals->meals;
        return view('meals.index')->with(['meals' => $meals]);
    }

    public function create()
    {
        return view('daily_meals.create');
    }

    public function store(DailyMealRequest $request)
    {
        if ($request->has('target_calories')) {
            $target = $request->get('target_calories');
            if($target != null)
            {
                $calories = $request->get('daily_calories');
//            dd($target, $calories);
                if ($target < $calories) {
                    Alert::error('Warning', 'You\'re reached over the daily calorie target ');
                    return redirect()->back();
                }
            }

        }
        $dailyMeal = DailyMeal::create($request->input());
        $dailyMeal->update();
        $dailyMeal->meals()->sync($request->get('meals'));
        $dailyMeal->save();
        if ($request->has($request->get('meals'))) {
            $dailyMeal->meals()->sync([$request->get('meals')]);
        }
        Alert::success('Great!', 'Daily meal created successfully!');
        return redirect(route('daily_meals.index'));
    }

    public function edit($id)
    {
        $dailyMeal = DailyMeal::find($id);
        return view('daily_meals.edit')->with(['dailyMeal' => $dailyMeal]);
    }

    public function update(DailyMealRequest $request, $id)
    {
        $data = $request->except('_method', '_token');
        $dailyMeal = DailyMeal::find($id);
        if ($dailyMeal) {
            if ($dailyMeal->target_calories) {
                $target = $dailyMeal->target_calories;
                $calories = $dailyMeal->daily_calories;
                if ($target < $calories) {
                    Alert::error('Warning', 'You\'re reached over the daily calorie target ');
                    return redirect()->back();
                }
            }
            $dailyMeal->meals()->sync($request->get('meals'));
            $dailyMeal->update($data);
        }
        Alert::success('Great!', 'Daily Meal Plan updated successfully!');
        return redirect()->action([DailyMealController::class, 'index']);
    }

    public function delete($id)
    {
        $dailyMeal = DailyMeal::find($id);
        if (!$dailyMeal) {
            Alert::error('Oops!', 'Daily meal plan not not found!');
            return redirect()->back();
        }
        if ($dailyMeal->diets()->exists()) {
            Alert::error('Oops!', 'Daily Plan couldn\'t be deleted due to related record/s!');
            return redirect()->back();
        }
        $dailyMeal->delete();
        Alert::success('Great!', 'The meal day was deleted successfully!');
        return redirect()->action([DailyMealController::class, 'index']);
    }
}
