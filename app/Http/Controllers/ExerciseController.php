<?php

namespace App\Http\Controllers;

use App\Http\Requests\ExerciseRequest;
use App\Models\Exercise;
use Illuminate\Http\Request;

class ExerciseController extends Controller
{
    public function index()
    {
        $exercises = Exercise::all();
        return view('exercises.index', compact('exercises'));
    }

    public function create()
    {
        return view('exercises.create');
    }

    public function store(ExerciseRequest $request)
    {
        $data =  $request->except('_method', '_token');
        Exercise::create($data);
        return redirect(route('exercises.index'));
    }

    public function edit($id)
    {
        $exercise = Exercise::find($id);
        return view('exercises.edit', compact('exercise'));
    }

    public function update(ExerciseRequest $request, $id)
    {
        $data = $request->except('_method', '_token');
        $exercise = Exercise::find($id);
        if ($exercise)
        {
            $exercise->update($data);
        }
        return redirect()->action([ExerciseController::class, 'index']);
    }

    public function delete($id)
    {
        $exercise = Exercise::find($id);
        if($exercise)
        {
            $exercise->delete();
            return redirect()->back();
        }
    }
}
