<?php

namespace App\Http\Controllers;

use App\Http\Requests\TrainerRequest;
use App\Models\Trainer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class TrainerController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:trainer-view', ['only' => ['index']]);
        $this->middleware('permission:trainer-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:trainer-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:trainer-delete', ['only' => ['delete']]);
    }
    public function index()
    {
        $trainers = Trainer::all();
        return view('trainers.index')->with(['trainers' => $trainers]);
    }

    public function create()
    {
        $users = User::leftJoin('trainers', 'users.id', '=', 'trainers.user_id')
            ->where('trainers.id', null)
            ->select('users.*')
            ->get();
        return view('trainers.create')->with(['users' => $users]);
    }

    public function store(Request $request)
    {
        $rules = array(
            'user_id'      => 'required',
            'address'      => 'required',
            'email'        => 'required|email',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect(route('trainers.create'))->withErrors($validator)->withInput($request->all());
        } else {
            $user = User::findOrFail($request->user_id);

            $trainer = Trainer::create([
                'user_id'      => $user->id,
                'trainer_name' => $user->first_name . " " . $user->last_name,
                'address'      => $request->get('address'),
                'email'        => $request->get('email'),
                'status'       => $request->get('status'),
            ]);

            switch ($trainer->status) {
                case 'suspended':
                    $user->removeRole('trainer');
                    $user->assignRole('suspended-trainer');
                    break;
                case 'active':
                case 'inactive':
                    $user->assignRole('trainer');
                    break;
            }
            $user->update();
        }

        Alert::success('Great!', 'Trainer created successfully!');
        return redirect(route('trainers.index'));
    }

    public function edit($id)
    {
        $trainer = Trainer::find($id);
        return view('trainers.edit')->with(['trainer' => $trainer]);
    }

    public function update(Request $request, $id)
    {
        $trainer = Trainer::findOrFail($id);
        $rules = array(
            'address' => 'required',
            'email'   => 'required|email',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect(route('trainers.edit'))->withErrors($validator)->withInput($request->all());
        } else {
            $trainer->address = $request->get('address');
            $trainer->email = $request->get('email');
            $trainer->status = $request->get('status');
            $trainer->update();

            $user = $trainer->user;
            switch ($trainer->status) {
                case 'suspended':
                    if ($user->hasRole('trainer')) {
                        $user->removeRole('trainer');
                    }
                    $user->assignRole('suspended-trainer');
                    break;
                case 'active':
                case 'inactive':
                    if ($user->hasRole('suspended-trainer')) {
                        $user->removeRole('suspended-trainer');
                    }
                    $user->assignRole('trainer');
                    break;
            }

            Alert::success('Great!', 'Dealer updated successfully!');
            return redirect()->action([TrainerController::class, 'index']);
        }
    }

    public function delete($id)
    {
        $trainer = Trainer::findOrFail($id);
        $userId = $trainer->user_id;
        $user = User::findOrFail($userId);
        $user->removeRole('dealer');
            $trainer->delete();

            Alert::success('Great!', 'Dealer deleted successfully!');
            return redirect()->action([TrainerController::class, 'index']);
        }
}
