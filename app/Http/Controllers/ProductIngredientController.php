<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductIngredientRequest;
use App\Models\ProductIngredient;
use Database\Seeders\PermissionSeeder;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ProductIngredientController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:ingredient-view', ['only' => ['index']]);
        $this->middleware('permission:ingredient-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:ingredient-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:ingredient-delete', ['only' => ['delete']]);
    }
    public function index()
    {
        $ingredients = ProductIngredient::all();
        return view('product_ingredients.index')->with(['ingredients' => $ingredients]);
    }

    public function create()
    {
        return view('product_ingredients.create');
    }

    public function store(ProductIngredientRequest $request)
    {
        $data = $request->except('_method', '_token');
        ProductIngredient::create($data);

        Alert::success('Great!', 'Ingredient created successfully!');
        return redirect(route('product_ingredients.index'));
    }

    public function edit($id)
    {
        $ingredient = ProductIngredient::find($id);
        return view('product_ingredients.edit')->with(['ingredient' => $ingredient]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->except('_method', '_token');
        $ingredient = ProductIngredient::find($id);
        if($ingredient)
        {
            if ($ingredient->amount < 0)
            {
                Alert::error('Wait a minute', 'You are entering wrong data! Please Try again');
                return redirect()->back();
            }
            $ingredient->update($data);
        }
        Alert::success('Great!', 'Ingredient updated successfully!');
        return redirect()->action([ProductIngredientController::class, 'index']);
    }

    public function delete($id)
    {
        $ingredient = ProductIngredient::find($id);
        if(!$ingredient)
        {
            Alert::error('Oops!', 'Ingredient not found!');
            return redirect()->back();
        }
        if ($ingredient->recipes()->exists() || $ingredient->meals()->exists()) {
            Alert::error('Oops!', 'Ingredient couldn\'t be deleted due to related record/s!');
            return redirect()->back();
        }
        $ingredient->delete();
        Alert::success('Great!', 'Ingredient deleted successfully!');
        return redirect()->back();
    }
}
