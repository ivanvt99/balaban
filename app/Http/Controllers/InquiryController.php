<?php

namespace App\Http\Controllers;

use App\Http\Requests\InquiryRequest;
use App\Models\Inquiry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class InquiryController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:inquiry-view', ['only' => ['index']]);
//        $this->middleware('permission:inquiry-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:inquiry-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:inquiry-delete', ['only' => ['delete']]);
    }
    public function index()
    {
        $inquiries = Inquiry::all();
        return view('inquiries.index')->with(['inquiries' => $inquiries]);
    }

    public function show($id)
    {
        $inquiry = Inquiry::find($id);
        if (!$inquiry) {
            Alert::error('Oops!', 'Inquiry not found!');
            return redirect()->back();
        }

        $inquiry->was_seen = true;
        $inquiry->save();

        return view('inquiries.show')->with(['inquiry' => $inquiry]);
    }

    public function create()
    {
        return view('inquiries.create');
    }

    public function store(InquiryRequest $request)
    {
        $data = $request->except('_method', '_token');
        Inquiry::create($data);
        if (!Auth::user())
        {
            Alert::success('Thank You!', 'We will answer you as soon as possible!');
            return redirect()->back();
        }
        if(Auth::user()->hasRole('client'))
        {
            Alert::success('Thank You!', 'We will answer you as soon as possible!');
            return redirect()->back();
        }
        Alert::success('Great!', 'Inquiry created successfully!');
        return redirect(route('inquiries.index'));
    }
    public function edit($id)
    {
        $inquiry = Inquiry::find($id);
        return view('inquiries.edit')->with(['inquiry' => $inquiry]);
    }

    public function update(InquiryRequest $request, $id)
    {
        $inquiry = Inquiry::find($id);
        if ($inquiry)
        {
            $data = $request->except('_method', '_token');
            $inquiry->update($data);
        }
        Alert::success('Great!', 'Inquiry updated successfully!');
        return redirect()->action([InquiryController::class, 'index']);
    }

    public function delete($id)
    {
        $inquiry = Inquiry::find($id);
        if (!$inquiry)
        {
            Alert::error('Oops!', 'Inquiry not found!');
            return redirect()->back();
        }
        $inquiry->delete();
        Alert::success('Great!', 'Inquiry deleted successfully!');
        return redirect()->back();
    }

}
