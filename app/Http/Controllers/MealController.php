<?php

namespace App\Http\Controllers;

use App\Http\Requests\MealRequest;
use App\Models\Meal;
use Database\Seeders\ProductIngredientSeeder;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class MealController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:meal-view', ['only' => ['index']]);
        $this->middleware('permission:meal-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:meal-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:meal-delete', ['only' => ['delete']]);
    }
    public function index()
    {
        $meals = Meal::all();
        return view('meals.index')->with(['meals' => $meals]);
    }

    public function create()
    {
        return view('meals.create');
    }

    public function store(MealRequest $request)
    {
        $meal = Meal::create($request->input());
        $meal->update();
        $meal->productIngredients()->sync($request->get('ingredients'));
        $meal->recipes()->sync($request->get('recipes'));
        $meal->supplementPortions()->sync($request->get('portions'));
        $meal->save();
        if ($request->has($request->get('ingredients'))) {
            $meal->productIngredients()->sync([$request->get('ingredients')]);
        }
        if ($request->has($request->get('portions'))) {
            $meal->supplementPortions()->sync([$request->get('portions')]);
        }
        if ($request->has($request->get('recipes'))) {
            $meal->recipes()->sync([$request->get('recipes')]);
        }
        Alert::success('Great!', 'Meal created successfully!');
        return redirect(route('meals.index'));
    }

    public function edit($id)
    {
        $meal = Meal::find($id);
        return view('meals.edit')->with(['meal' => $meal]);
    }

    public function update(MealRequest $request, $id)
    {
        $data = $request->except('_method', '_token');
        $meal = Meal::find($id);
        if ($meal) {
            $meal->productIngredients()->sync($request->get('ingredients'));
            $meal->recipes()->sync($request->get('recipes'));
            $meal->supplementPortions()->sync($request->get('portions'));
            $meal->update($data);
        }
        Alert::success('Great!', 'Meal updated successfully!');
        return redirect()->action([MealController::class, 'index']);
    }

    public function delete($id)
    {
        $meal = Meal::find($id);
        if (!$meal) {
            Alert::error('Oops!', 'Meal not found!');
            return redirect()->back();
        }
        if ($meal->dailyMeals()->exists()) {
            Alert::error('Oops!', 'Meal couldn\'t be deleted due to related record/s!');
            return redirect()->back();
        }
        $meal->delete();
        Alert::success('Great!', 'Meal deleted successfully!');
        return redirect()->back();
    }
}
