<?php

namespace App\Http\Controllers;

use App\Http\Requests\SupplementPortionRequest;
use App\Models\SupplementPortion;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class SupplementPortionController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:supplement-portion-view', ['only' => ['index']]);
        $this->middleware('permission:supplement-portion-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:supplement-portion-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:supplement-portion-delete', ['only' => ['delete']]);
    }
    public function index()
    {
        $portions = SupplementPortion::all();
        return view('supplement_portions.index')->with(['portions' => $portions]);
    }

    public function create()
    {
        return view('supplement_portions.create');
    }

    public function store(SupplementPortionRequest $request)
    {
        $data = $request->except('_method', '_token');
        SupplementPortion::create($data);

        Alert::success('Great!', 'Portion created successfully!');
        return redirect(route('supplement_portions.index'));
    }

    public function edit($id)
    {
        $portion = SupplementPortion::find($id);
        return view('supplement_portions.edit')->with(['portion' => $portion]);
    }

    public function update(SupplementPortionRequest $request, $id)
    {
        $data = $request->except('_method', '_token');
        $portion = SupplementPortion::find($id);
        if($portion)
        {
            if ($portion->amount < 0)
            {
                Alert::error('Wait a minute', 'You are entering wrong data! Please Try again');
                return redirect()->back();
            }
            $portion->update($data);
        }
        Alert::success('Great!', 'Ingredient updated successfully!');
        return redirect()->action([SupplementPortionController::class, 'index']);
    }

    public function delete($id)
    {
        $portion = SupplementPortion::find($id);
        if (!$portion)
        {
            Alert::error('Oops!', 'Ingredient not found!');
            return redirect()->back();
        }
        $portion->delete();
        Alert::success('Great!', 'Ingredient deleted successfully!');
        return redirect()->back();
    }
}
